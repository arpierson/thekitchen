-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema thekitchen
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `thekitchen` ;

-- -----------------------------------------------------
-- Schema thekitchen
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `thekitchen` DEFAULT CHARACTER SET utf8 ;
USE `thekitchen` ;

-- -----------------------------------------------------
-- Table `thekitchen`.`Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`Country` ;

CREATE TABLE IF NOT EXISTS `thekitchen`.`Country` (
  `code` VARCHAR(2) NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`code`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `thekitchen`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`User` ;

CREATE TABLE IF NOT EXISTS `thekitchen`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL UNIQUE,
  `password` VARCHAR(150) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `countryCode` VARCHAR(2) NOT NULL,
  `picture` MEDIUMBLOB NULL,
  `dateAdded` DATETIME NOT NULL,
  `dateModified` DATETIME NULL,
  `dateDeleted` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `countryCode_idx` (`countryCode` ASC),
  CONSTRAINT `FK_User_Country`
    FOREIGN KEY (`countryCode`)
    REFERENCES `thekitchen`.`Country` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `thekitchen`.`Recipe`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`Recipe` ;

CREATE TABLE IF NOT EXISTS `thekitchen`.`Recipe` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `picture` MEDIUMBLOB NULL,
  `dateAdded` DATETIME NOT NULL,
  `dateModified` DATETIME NULL,
  `dateDeleted` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_Users_Recipes_idx` (`userId` ASC),
  CONSTRAINT `FK_Users_Recipes`
    FOREIGN KEY (`userId`)
    REFERENCES `thekitchen`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `thekitchen`.`Ingredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`Ingredient` ;

CREATE TABLE IF NOT EXISTS `thekitchen`.`Ingredient` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `dateAdded` DATETIME NOT NULL,
  `dateModified` DATETIME NULL,
  `dateDeleted` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_Ingredient_User_idx` (`userId` ASC),
  CONSTRAINT `FK_Ingredient_User`
    FOREIGN KEY (`userId`)
    REFERENCES `thekitchen`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `thekitchen`.`Recipe_Ingredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`Recipe_Ingredient` ;

CREATE TABLE IF NOT EXISTS `thekitchen`.`Recipe_Ingredient` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ingredientId` INT NOT NULL,
  `recipeId` INT NOT NULL,
  `measurement` VARCHAR(45) NOT NULL,
  `dateAdded` DATETIME NOT NULL,
  `dateModified` DATETIME NULL,
  `dateDeleted` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_Recipe_Ingredient_Ingredient_idx` (`ingredientId` ASC),
  INDEX `FK_Recipe_Ingredient_Recipe_idx` (`recipeId` ASC),
  CONSTRAINT `FK_Recipe_Ingredient_Ingredient`
    FOREIGN KEY (`ingredientId`)
    REFERENCES `thekitchen`.`Ingredient` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Recipe_Ingredient_Recipe`
    FOREIGN KEY (`recipeId`)
    REFERENCES `thekitchen`.`Recipe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `thekitchen`.`Instruction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`Instruction` ;

CREATE TABLE IF NOT EXISTS `thekitchen`.`Instruction` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `recipeId` INT NOT NULL,
  `step` INT NOT NULL,
  `instruction` VARCHAR(150) NOT NULL,
  `dateAdded` DATETIME NOT NULL,
  `dateModified` DATETIME NULL,
  `dateDeleted` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_Instructions_Recipe_idx` (`recipeId` ASC),
  CONSTRAINT `FK_Instructions_Recipe`
    FOREIGN KEY (`recipeId`)
    REFERENCES `thekitchen`.`Recipe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `thekitchen`.`Taste_Rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`Taste_Rating` ;

CREATE TABLE `Taste_Rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `recipeId` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_tasteRating_userId_recipeId` (`userId`,`recipeId`),
  KEY `FK_TasteRating_User_idx` (`userId`),
  KEY `FK_TasteRating_Recipe_idx` (`recipeId`),
  CONSTRAINT `FK_TasteRating_Recipe` FOREIGN KEY (`recipeId`) REFERENCES `Recipe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `thekitchen`.`Difficulty_Rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`Difficulty_Rating` ;

CREATE TABLE `Difficulty_Rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `recipeId` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_difficultyRating_userId_recipeId` (`userId`,`recipeId`),
  KEY `FK_DifficultyRating_User_idx` (`userId`),
  KEY `FK_DifficultyRating_Recipe_idx` (`recipeId`),
  CONSTRAINT `FK_DifficultyRating_Recipe` FOREIGN KEY (`recipeId`) REFERENCES `Recipe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_DifficultyRating_User` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `thekitchen`.`Recipe_Book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`Recipe_Book` ;

CREATE TABLE IF NOT EXISTS `thekitchen`.`Recipe_Book` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `dateAdded` DATETIME NOT NULL,
  `dateModified` DATETIME NULL,
  `dateDeleted` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `id_idx` (`userId` ASC),
  CONSTRAINT `id`
    FOREIGN KEY (`userId`)
    REFERENCES `thekitchen`.`User` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `thekitchen`.`Recipe_Book_Entry`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `thekitchen`.`Recipe_Book_Entry` ;

CREATE TABLE IF NOT EXISTS `thekitchen`.`Recipe_Book_Entry` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `recipeBookId` INT NOT NULL,
  `recipeId` INT NOT NULL,
  `dateAdded` DATETIME NOT NULL,
  `dateModified` DATETIME NULL,
  `dateDeleted` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `id_idx` (`recipeBookId` ASC),
  INDEX `recipeId_idx` (`recipeId` ASC),
  CONSTRAINT `recipeBookId`
    FOREIGN KEY (`recipeBookId`)
    REFERENCES `thekitchen`.`Recipe_Book` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `recipeId`
    FOREIGN KEY (`recipeId`)
    REFERENCES `thekitchen`.`Recipe` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Seed Data For 'Country' Table
-- -----------------------------------------------------
INSERT INTO `Country` (`code`, `name`) VALUES
('AF', 'Afghanistan'),
('AL', 'Albania'),
('DZ', 'Algeria'),
('AS', 'American Samoa'),
('AD', 'Andorra'),
('AO', 'Angola'),
('AI', 'Anguilla'),
('AQ', 'Antarctica'),
('AG', 'Antigua and Barbuda'),
('AR', 'Argentina'),
('AM', 'Armenia'),
('AW', 'Aruba'),
('AU', 'Australia'),
('AT', 'Austria'),
('AZ', 'Azerbaijan'),
('BS', 'Bahamas'),
('BH', 'Bahrain'),
('BD', 'Bangladesh'),
('BB', 'Barbados'),
('BY', 'Belarus'),
('BE', 'Belgium'),
('BZ', 'Belize'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BT', 'Bhutan'),
('BO', 'Bolivia'),
('BA', 'Bosnia and Herzegovina'),
('BW', 'Botswana'),
('BV', 'Bouvet Island'),
('BR', 'Brazil'),
('IO', 'British Indian Ocean Territory'),
('BN', 'Brunei Darussalam'),
('BG', 'Bulgaria'),
('BF', 'Burkina Faso'),
('BI', 'Burundi'),
('KH', 'Cambodia'),
('CM', 'Cameroon'),
('CA', 'Canada'),
('CV', 'Cape Verde'),
('KY', 'Cayman Islands'),
('CF', 'Central African Republic'),
('TD', 'Chad'),
('CL', 'Chile'),
('CN', 'China'),
('CX', 'Christmas Island'),
('CC', 'Cocos (Keeling) Islands'),
('CO', 'Colombia'),
('KM', 'Comoros'),
('CG', 'Congo'),
('CD', 'Congo, the Democratic Republic of the'),
('CK', 'Cook Islands'),
('CR', 'Costa Rica'),
('CI', 'Cote D''Ivoire'),
('HR', 'Croatia'),
('CU', 'Cuba'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DK', 'Denmark'),
('DJ', 'Djibouti'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('EC', 'Ecuador'),
('EG', 'Egypt'),
('SV', 'El Salvador'),
('GQ', 'Equatorial Guinea'),
('ER', 'Eritrea'),
('EE', 'Estonia'),
('ET', 'Ethiopia'),
('FK', 'Falkland Islands (Malvinas)'),
('FO', 'Faroe Islands'),
('FJ', 'Fiji'),
('FI', 'Finland'),
('FR', 'France'),
('GF', 'French Guiana'),
('PF', 'French Polynesia'),
('TF', 'French Southern Territories'),
('GA', 'Gabon'),
('GM', 'Gambia'),
('GE', 'Georgia'),
('DE', 'Germany'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GR', 'Greece'),
('GL', 'Greenland'),
('GD', 'Grenada'),
('GP', 'Guadeloupe'),
('GU', 'Guam'),
('GT', 'Guatemala'),
('GN', 'Guinea'),
('GW', 'Guinea-Bissau'),
('GY', 'Guyana'),
('HT', 'Haiti'),
('HM', 'Heard Island and Mcdonald Islands'),
('VA', 'Holy See (Vatican City State)'),
('HN', 'Honduras'),
('HK', 'Hong Kong'),
('HU', 'Hungary'),
('IS', 'Iceland'),
('IN', 'India'),
('ID', 'Indonesia'),
('IR', 'Iran, Islamic Republic of'),
('IQ', 'Iraq'),
('IE', 'Ireland'),
('IL', 'Israel'),
('IT', 'Italy'),
('JM', 'Jamaica'),
('JP', 'Japan'),
('JO', 'Jordan'),
('KZ', 'Kazakhstan'),
('KE', 'Kenya'),
('KI', 'Kiribati'),
('KP', 'Korea, Democratic People''s Republic of'),
('KR', 'Korea, Republic of'),
('KW', 'Kuwait'),
('KG', 'Kyrgyzstan'),
('LA', 'Lao People''s Democratic Republic'),
('LV', 'Latvia'),
('LB', 'Lebanon'),
('LS', 'Lesotho'),
('LR', 'Liberia'),
('LY', 'Libyan Arab Jamahiriya'),
('LI', 'Liechtenstein'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('MO', 'Macao'),
('MK', 'Macedonia, the Former Yugoslav Republic of'),
('MG', 'Madagascar'),
('MW', 'Malawi'),
('MY', 'Malaysia'),
('MV', 'Maldives'),
('ML', 'Mali'),
('MT', 'Malta'),
('MH', 'Marshall Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MU', 'Mauritius'),
('YT', 'Mayotte'),
('MX', 'Mexico'),
('FM', 'Micronesia, Federated States of'),
('MD', 'Moldova, Republic of'),
('MC', 'Monaco'),
('MN', 'Mongolia'),
('MS', 'Montserrat'),
('MA', 'Morocco'),
('MZ', 'Mozambique'),
('MM', 'Myanmar'),
('NA', 'Namibia'),
('NR', 'Nauru'),
('NP', 'Nepal'),
('NL', 'Netherlands'),
('AN', 'Netherlands Antilles'),
('NC', 'New Caledonia'),
('NZ', 'New Zealand'),
('NI', 'Nicaragua'),
('NE', 'Niger'),
('NG', 'Nigeria'),
('NU', 'Niue'),
('NF', 'Norfolk Island'),
('MP', 'Northern Mariana Islands'),
('NO', 'Norway'),
('OM', 'Oman'),
('PK', 'Pakistan'),
('PW', 'Palau'),
('PS', 'Palestinian Territory, Occupied'),
('PA', 'Panama'),
('PG', 'Papua New Guinea'),
('PY', 'Paraguay'),
('PE', 'Peru'),
('PH', 'Philippines'),
('PN', 'Pitcairn'),
('PL', 'Poland'),
('PT', 'Portugal'),
('PR', 'Puerto Rico'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('SH', 'Saint Helena'),
('KN', 'Saint Kitts and Nevis'),
('LC', 'Saint Lucia'),
('PM', 'Saint Pierre and Miquelon'),
('VC', 'Saint Vincent and the Grenadines'),
('WS', 'Samoa'),
('SM', 'San Marino'),
('ST', 'Sao Tome and Principe'),
('SA', 'Saudi Arabia'),
('SN', 'Senegal'),
('CS', 'Serbia and Montenegro'),
('SC', 'Seychelles'),
('SL', 'Sierra Leone'),
('SG', 'Singapore'),
('SK', 'Slovakia'),
('SI', 'Slovenia'),
('SB', 'Solomon Islands'),
('SO', 'Somalia'),
('ZA', 'South Africa'),
('GS', 'South Georgia and the South Sandwich Islands'),
('ES', 'Spain'),
('LK', 'Sri Lanka'),
('SD', 'Sudan'),
('SR', 'Suriname'),
('SJ', 'Svalbard and Jan Mayen'),
('SZ', 'Swaziland'),
('SE', 'Sweden'),
('CH', 'Switzerland'),
('SY', 'Syrian Arab Republic'),
('TW', 'Taiwan, Province of China'),
('TJ', 'Tajikistan'),
('TZ', 'Tanzania, United Republic of'),
('TH', 'Thailand'),
('TL', 'Timor-Leste'),
('TG', 'Togo'),
('TK', 'Tokelau'),
('TO', 'Tonga'),
('TT', 'Trinidad and Tobago'),
('TN', 'Tunisia'),
('TR', 'Turkey'),
('TM', 'Turkmenistan'),
('TC', 'Turks and Caicos Islands'),
('TV', 'Tuvalu'),
('UG', 'Uganda'),
('UA', 'Ukraine'),
('AE', 'United Arab Emirates'),
('GB', 'United Kingdom'),
('US', 'United States'),
('UM', 'United States Minor Outlying Islands'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VU', 'Vanuatu'),
('VE', 'Venezuela'),
('VN', 'Viet Nam'),
('VG', 'Virgin Islands, British'),
('VI', 'Virgin Islands, U.S.'),
('WF', 'Wallis and Futuna'),
('EH', 'Western Sahara'),
('YE', 'Yemen'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');


-- -----------------------------------------------------
-- Seed Data For 'User' Table
-- -----------------------------------------------------
INSERT INTO `User` (`email`, `password`, `firstname`, `lastname`, `countryCode`, `dateAdded`) VALUES
('jrobbin4@my.westga.edu', '$2b$10$73Um876XTCzfU7cCA5jOAuEDKMrKe9gYjdjTTRE.ykRISUjb2wxIu', 'Jason', 'Robbins', 'US', NOW()),
('bdepoin1@my.westga.edu', '$2b$10$73Um876XTCzfU7cCA5jOAuEDKMrKe9gYjdjTTRE.ykRISUjb2wxIu', 'Ben', 'De Point', 'US', NOW());


-- -----------------------------------------------------
-- Seed Data For 'Ingredient' Table
-- -----------------------------------------------------
INSERT INTO `Ingredient` (`userId`, `name`, `dateAdded`) VALUES
(1, 'CHICKEN', NOW()),
(1, 'BEEF', NOW()),
(1, 'SALT', NOW()),
(1, 'PEPPER', NOW()),
(1, 'TOMATOS', NOW()),
(1, 'CARROTS', NOW()),
(1, 'APPLES', NOW()),
(1, 'PEACHES', NOW()),
(1, 'ORANGE', NOW()),
(1, 'CHEESE', NOW()),
(1, 'BREAD', NOW()),
(1, 'PASTA', NOW()),
(1, 'CORN', NOW()),
(1, 'BROCCOLI', NOW()),
(1, 'PEAS', NOW()),
(1, 'FISH', NOW()),
(1, 'SALMON', NOW()),
(1, 'TUNA', NOW()),
(1, 'SARDINES', NOW()),
(1, 'HAM', NOW()),
(1, 'BACON', NOW()),
(1, 'EGGS', NOW()),
(1, 'SUGAR', NOW()),
(1, 'BROWN SUGAR', NOW()),
(1, 'POWDER SUGAR', NOW()),
(1, 'BUTTER', NOW()),
(1, 'WATER', NOW()),
(1, 'OIL', NOW()),
(1, 'VEGETABLE OIL', NOW()),
(1, 'OLIVE OIL', NOW()),
(1, 'EXTRA VIRGIN OLIVE OIL', NOW()),
(1, 'COCONUT', NOW()),
(1, 'COCONUT OIL', NOW()),
(1, 'EXTRA VIRGIN COCONUT OIL', NOW()),
(1, 'BEER', NOW()),
(1, 'STRAWBERRIES', NOW()),
(1, 'BLUEBERRIES', NOW()),
(1, 'RASPBERRIES', NOW()),
(1, 'BANANA', NOW()),
(1, 'MILK', NOW()),
(1, 'CONDENSED MILK', NOW()),
(1, 'CHICKEN BROTH', NOW()),
(1, 'BONE BROTH', NOW()),
(1, 'BEEF BROTH', NOW()),
(1, 'RED PEPPER', NOW()),
(1, 'CHICKEN BREAST', NOW()),
(1, 'STEAK', NOW()),
(1, 'MAYONNAISE', NOW()),
(1, 'KETCHUP', NOW()),
(1, 'MUSTARD', NOW()),
(1, 'BARBECUE SAUCE', NOW()),
(1, 'SPICY MUSTARD', NOW()),
(1, 'SQUASH', NOW()),
(1, 'GREEN BEANS', NOW()),
(1, 'BLACK BEANS', NOW()),
(1, 'COLLARD GREENS', NOW()),
(1, 'PORK', NOW()),
(1, 'PULLED PORK', NOW()),
(1, 'BLACK EYED PEAS', NOW()),
(1, 'LIMA BEANS', NOW()),
(1, 'LENTILS', NOW()),
(1, 'TURKEY', NOW()),
(1, 'CHOCOLATE', NOW()),
(1, 'VANILLA', NOW()),
(1, 'CARAMEL', NOW()),
(1, 'RED BEANS', NOW()),
(1, 'PEANUTS', NOW()),
(1, 'ALMONDS', NOW()),
(1, 'CASHEWS', NOW()),
(1, 'NUTS', NOW()),
(1, 'WALNUTS', NOW()),
(1, 'PECANS', NOW()),
(1, 'VINEGAR', NOW()),
(1, 'RANCH DRESSING', NOW()),
(1, 'FRENCH DRESSING', NOW()),
(1, '1000 ISLAND DRESSING', NOW()),
(1, 'VINAIGRETTE', NOW()),
(1, 'VINAIGRETTE DRESSING', NOW()),
(1, 'POTATOES', NOW()),
(1, 'RED POTATOES', NOW()),
(1, 'WHITE POTATOES', NOW()),
(1, 'MASHED POTATOES', NOW()),
(1, 'ALLSPICE', NOW()),
(1, 'ANISE', NOW()),
(1, 'BAKING POWDER', NOW()),
(1, 'BASIL', NOW()),
(1, 'BAY LEAVES', NOW()),
(1, 'BLACK PEPPER', NOW()),
(1, 'CARAWAY SEED', NOW()),
(1, 'PUMPKIN SEED', NOW()),
(1, 'CARDAMOM', NOW()),
(1, 'CAYANNE PEPPER', NOW()),
(1, 'CHILI POWDER', NOW()),
(1, 'CHINESE FIVE SPICE', NOW()),
(1, 'CHIVES', NOW()),
(1, 'CINNAMON', NOW()),
(1, 'CORN STARCH', NOW()),
(1, 'CREAM OF TARTAR', NOW()),
(1, 'GROUND CUMIN', NOW()),
(1, 'CURRY POWDER', NOW()),
(1, 'CUMIN', NOW()),
(1, 'DILL', NOW()),
(1, 'FENNEL SEED', NOW()),
(1, 'GARLIC', NOW()),
(1, 'GARLIC POWDER', NOW()),
(1, 'GINGER', NOW()),
(1, 'KOSHER SALT', NOW()),
(1, 'MACE', NOW()),
(1, 'MARJORAM', NOW()),
(1, 'NUTMEG', NOW()),
(1, 'OLD BAY SEASONING', NOW()),
(1, 'ONION', NOW()),
(1, 'ONION POWDER', NOW()),
(1, 'OREGANO', NOW()),
(1, 'PAPRIKA', NOW()),
(1, 'PEPPERCORNS', NOW()),
(1, 'ROSEMARY', NOW()),
(1, 'SAFFRON', NOW()),
(1, 'SAGE', NOW()),
(1, 'SEA SALT', NOW()),
(1, 'SESAME SEEDS', NOW()),
(1, 'TARRAGON', NOW()),
(1, 'THYME', NOW()),
(1, 'TURMERIC', NOW()),
(1, 'VANILLA EXTRACT', NOW()),
(1, 'WINE', NOW()),
(1, 'WHITE WINE', NOW()),
(1, 'RED WINE', NOW()),
(1, 'COOKING WINE', NOW()),
(1, 'RICE COOKING WINE', NOW()),
(1, 'SOY SAUCE', NOW()),
(1, 'TOBASCO SAUCE', NOW()),
(1, 'SRIRACHA', NOW()),
(1, 'BALSAMIC', NOW()),
(1, 'BALSAMIC VINEGAR', NOW()),
(1, 'TOMATO PASTE', NOW()),
(1, 'BAKING SODA', NOW()),
(1, 'BREADCRUMBS', NOW()),
(1, 'CHOCOLATE CHIPS', NOW()),
(1, 'COCOA', NOW()),
(1, 'FLOUR', NOW()),
(1, 'OATMEAL', NOW()),
(1, 'RICE', NOW()),
(1, 'SHORTENING', NOW()),
(1, 'ONION SOUP', NOW()),
(1, 'VEGETABLE SOUP', NOW()),
(1, 'YEAST', NOW()),
(1, 'LEMONS', NOW()),
(1, 'MUSHROOMS', NOW()),
(1, 'RED PEPPERS', NOW()),
(1, 'PEPPERS', NOW()),
(1, 'JALAPENOS', NOW()),
(1, 'CONDENSED SOUP', NOW()),
(1, 'TOMATO SAUCE', NOW()),
(1, 'HONEY', NOW()),
(1, 'HOT SAUCE', NOW()),
(1, 'OLIVES', NOW()),
(1, 'PICKLES', NOW()),
(1, 'SALSA', NOW()),
(1, 'PIMENTOS', NOW()),
(1, 'WORCESTERSHIRE SAUCE', NOW()),
(1, 'LIMES', NOW()),
(1, 'LIME JUICE', NOW()),
(1, 'LEMON JUICE', NOW()),
(1, 'RAMONE LETTUCE', NOW()),
(1, 'ORANGE JUICE', NOW()),
(1, 'APPLE JUICE', NOW()),
(1, 'GRAPE JUICE', NOW()),
(1, 'GRAPES', NOW()),
(1, 'SALTINE CRACKERS', NOW()),
(1, 'PIZZA SAUCE', NOW()),
(1, 'MOZZARELLA CHEESE, QUARTERED', NOW()),
(1, 'PEPPERONI', NOW()),
(1, 'SLICED PEPPERONI', NOW()),
(1, 'ROLLS', NOW()),
(1, 'CRESCENT ROLLS', NOW()),
(1, 'SOUR CREAM', NOW()),
(1, 'CREAM CHEESE', NOW()),
(1, 'BACON SLICES', NOW()),
(1, 'LETTUCE', NOW());


-- -----------------------------------------------------
-- Seed Data For 'Recipe' Table
-- -----------------------------------------------------
INSERT INTO `Recipe` (`userId`, `name`, `description`, `dateAdded`) VALUES
(1, 'Pizza Nibblers', 'Bite sized pizza crackers. A perfect finger food snack.', NOW()),
(1, 'Bacon Rollups', 'Bacon and cream cheese rolled into a crescent! What more is there to say?', NOW());


-- -----------------------------------------------------
-- Seed Data For 'Recipe_Ingredient' Table
-- -----------------------------------------------------
INSERT INTO `Recipe_Ingredient` (`ingredientId`, `recipeId`, `measurement`, `dateAdded`) VALUES
(169, 1, '4', NOW()),
(170, 1, '3 tablespoons', NOW()),
(171, 1, '1 slice', NOW()),
(172, 1, '4 slices', NOW()),
(175, 2, '8', NOW()),
(177, 2, '1/2 cup', NOW()),
(21, 2, '12 slices', NOW());


-- -----------------------------------------------------
-- Seed Data For 'Instruction' Table
-- -----------------------------------------------------
INSERT INTO `Instruction` (`recipeId`, `step`, `instruction`, `dateAdded`) VALUES
(1, 1, 'Spread one side of each cracker with pizze sauce.', NOW()),
(1, 2, 'Top each cracker with a piece of cheese.', NOW()),
(1, 3, 'Top each cracker with a slice of pepperoni.', NOW()),
(1, 4, 'Arrange crackers on a microwaveable safe plate.', NOW()),
(1, 5, 'Microwave for 30 seconds.', NOW()),
(2, 1, 'Cook bacon until crisp.', NOW()),
(2, 2, 'Crumble bacon.', NOW()),
(2, 3, 'Unroll crescent rolls and seperate triangles.', NOW()),
(2, 4, 'Spread rolls with cream cheese.', NOW()),
(2, 5, 'Top with crumbled bacon.', NOW()),
(2, 6, 'Cut triangles into 2 equal wedges and roll up.', NOW()),
(2, 7, 'Grease a baking sheet.', NOW()),
(2, 8, 'Place rollups on baking sheet.', NOW()),
(2, 9, 'Bake at 375 degrees until golden brown (about 12 to 15 minutes).', NOW());


-- -----------------------------------------------------
-- Seed Data For 'Recipe_Book' Table
-- -----------------------------------------------------
INSERT INTO `Recipe_Book` (`userId`, `name`, `dateAdded`) VALUES
(1, 'Recipe Book 1', NOW()),
(1, 'Recipe Book 2', NOW());


-- -----------------------------------------------------
-- Seed Data For 'Recipe_Book_Entry' Table
-- -----------------------------------------------------
INSERT INTO `Recipe_Book_Entry` (`recipeBookId`, `recipeId`, `dateAdded`) VALUES
(1, 1, NOW()),
(1, 2, NOW());


-- -----------------------------------------------------
-- Create User with SELECT, INSERT, and UPDATE access
-- -----------------------------------------------------
CREATE USER IF NOT EXISTS 'azure'@'localhost' IDENTIFIED BY 'f3SvgbdMmUDWLd2w';
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE ON thekitchen.* TO 'azure'@'localhost';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
