using System;
using System.Data;
using System.IO;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchen.Utilities
{
    public static class Hashing
    {
        /// <summary>
        /// Uses BCrypt to hash the value
        /// </summary>
        /// <param name="value"></param>
        /// <returns>The hashed value</returns>
        public static string GetHash(string value)
        {
            return BCrypt.Net.BCrypt.HashPassword(value);
            
        }

        /// <summary>
        /// Used to verify if a plain text entry matches a stored hash
        /// </summary>
        /// <param name="value">The plain text entry</param>
        /// <param name="hashedValue">The stored hash</param>
        /// <returns>True or False</returns>
        public static bool VerifyHash(string value, string hashedValue)
        {
            return BCrypt.Net.BCrypt.Verify(value, hashedValue);
        }
    }

    /// <summary>
    /// Retrieves data from the Country table
    /// </summary>
    public static class Country
    {
        /// <summary>
        /// Returns a DataTable of country codes and names
        /// </summary>
        /// <returns>Country codes and names</returns>
        public static DataTable GetCountryDataTable()
        {
            CountryDAL dal = new CountryDAL();
            DataTable table = dal.GetCountryDataTable();
            DataRow row = table.NewRow();
            row["code"] = "";
            row["name"] = "Select One";
            table.Rows.InsertAt(row, 0);
            return table;
        }

        /// <summary>
        /// Helper method to return the Country Name from its Code
        /// </summary>
        /// <param name="code">The Country's code</param>
        /// <returns>Country Name</returns>
        public static string GetCountryFromCode(string code)
        {
            CountryDAL dal = new CountryDAL();
            return dal.GetCountryFromCode(code);
        }
    }

    /// <summary>
    /// Returns a custom 404 page
    /// </summary>
    public class NotFoundViewResult : ViewResult
    {
        public NotFoundViewResult(string viewName)
        {
            ViewName = viewName;
            StatusCode = (int)HttpStatusCode.NotFound;
        }
    }

    /// <summary>
    /// Used for uploading profile and recipe images
    /// </summary>
    public static class Images
    {
        /// <summary>
        /// Converts IFormFile image uploads to a byte array for DB storage
        /// </summary>
        /// <param name="image">The uploaded image</param>
        /// <returns>A byte array</returns>
        public static byte[] ConvertUploadedImage(IFormFile image)
        {
            //Is IFormFile null?
            if (image == null || image.Length < 1)
            {
                return null;
            }

            //Is IformFile wrong content type?
            if (image.ContentType.ToLower() != "image/jpg" &&
                image.ContentType.ToLower() != "image/jpeg" &&
                image.ContentType.ToLower() != "image/pjpeg" &&
                image.ContentType.ToLower() != "image/gif" &&
                image.ContentType.ToLower() != "image/x-png" &&
                image.ContentType.ToLower() != "image/png")
            {
                return null;
            }

            //Is IformFile wrong extension type?
            if (Path.GetExtension(image.FileName).ToLower() != ".jpg"
                && Path.GetExtension(image.FileName).ToLower() != ".png"
                && Path.GetExtension(image.FileName).ToLower() != ".gif"
                && Path.GetExtension(image.FileName).ToLower() != ".jpeg")
            {
                return null;
            }

            //Convert IformFile
            string imageString;
            try
            {
                using var memoryStream = new MemoryStream();
                image.CopyTo(memoryStream);
                var fileBytes = memoryStream.ToArray();
                imageString = Convert.ToBase64String(fileBytes);
            }
            catch
            {
                return null;
            }
            
            return Convert.FromBase64String(imageString);
        }
    }

    public static class User
    {
        public static int GetUserIDFromEmail(string email)
        {
            UserDAL dal = new UserDAL();
            UserModel user = dal.GetUser(email);
            return user.UserId;
        }
    }

    public static class Recipe
    {
        public  const int pageSize = 24;

        public static string BuildRecipeOrderBy(
            string sortBy,
            string orderBy)
        {
            string filter = " ORDER BY";

            switch (sortBy)
            {
                case "difficulty":
                    filter += " difficultyRating";
                    break;
                case "taste":
                    filter += " tasteRating";
                    break;
                case "added":
                    filter += " dateAdded";
                    break;
                case "name":
                    filter += " name";
                    break;
                default:
                    filter += " dateAdded";
                    break;
            }

            switch (orderBy)
            {
                case "lowToHigh":
                    filter += " ASC";
                    break;
                case "highToLow":
                    filter += " DESC";
                    break;
                default:
                    filter += " DESC";
                    break;
            }

            return filter;
        }

        public static int SetPage(int page)
        {
            page -= 1;

            if (page > 0)
            {
                page = page * pageSize;
            }

            return page;
        }
    }
}
