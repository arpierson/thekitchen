using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using TheKitchen.DAL;
using System.Security.Claims;
using System.Collections.Generic;
using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using TheKitchen.Models;
using System.ComponentModel.DataAnnotations;
using MySqlConnector;

namespace TheKitchen.Controllers
{
    public class UserController : Controller
    {
        private readonly UserDAL userDal;
        
        /// <summary>
        /// Constructor for UserController
        /// </summary>
        public UserController()
        {
            this.userDal = new UserDAL();
        }

        /// <summary>
        /// For user login
        /// </summary>
        /// <param name="email">The user's email</param>
        /// <param name="password">The user's password</param>
        /// <returns>HTTP Code and Message</returns>
        [HttpPost("User/Login")]
        public async System.Threading.Tasks.Task<IActionResult> LoginAsync(
            string loginEmail,
            string loginPassword)
        {
            if (String.IsNullOrEmpty(loginEmail) || String.IsNullOrEmpty(loginPassword))
            {
                return Unauthorized("Invalid Email or Password");
            }

            try
            {
                UserModel user = this.userDal.AuthenticateUser(loginEmail, loginPassword);
                if (user != null)
                {
                    await this.BuildAuthenticationCookieAsync(user);

                    if (user.Image != null)
                    {
                        this.PutAvatarInSession(user.Image);
                    }

                    return Ok();
                }

                return Unauthorized("Invalid Email or Password");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong with your login. Please try again");
            }
            
        }

        /// <summary>
        /// Used for udpating user 
        /// </summary>
        /// <param name="firstName">The user's first name</param>
        /// <param name="lastName">The user's last name</param>
        /// <param name="email">The user's email</param>
        /// <param name="countryCode">The user's country code</param>
        /// <param name="password">The user's password</param>
        /// <param name="comfirmPassword">The user's password confirmation</param>
        /// <returns>HTTP Code and Message</returns>
        [HttpPost("User/Update")]
        public async System.Threading.Tasks.Task<IActionResult> UpdateUserAsync(
            string firstName,
            string lastName,
            string email,
            string countryCode,
            string password,
            string confirmPassword,
            IFormFile image)
        {
            if (password != confirmPassword)
            {
                return Unauthorized("Password and Confirm Password do not match");
            }

            try
            {
                byte[] imageByte = Utilities.Images.ConvertUploadedImage(image);

                UserModel newUser = this.BuildUserObject(
                    firstName,
                    lastName,
                    email,
                    countryCode,
                    password,
                    imageByte);

                UserModel oldUser = this.userDal.GetUser(User.FindFirst(ClaimTypes.Email).Value);
                if (oldUser.Image != null && newUser.Image == null)
                {
                    newUser.Image = oldUser.Image;
                }
                this.userDal.UpdateUser(oldUser, newUser);

                await this.BuildAuthenticationCookieAsync(newUser);

                if (imageByte != null)
                {
                    this.PutAvatarInSession(imageByte);
                }
            }
            catch (ArgumentException badArugment)
            {
                return BadRequest(badArugment.Message);
            }
            catch (MySqlException mySqlEx)
            {
                if (mySqlEx.Message.IndexOf("email") > 0)
                {
                    return BadRequest("That email already exists");
                }
                throw new Exception(mySqlEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong with your login. Please try again");
            }

            return new StatusCodeResult(200);
        }

        /// <summary>
        /// Used for new user Registration 
        /// </summary>
        /// <param name="firstName">The user's first name</param>
        /// <param name="lastName">The user's last name</param>
        /// <param name="email">The user's email</param>
        /// <param name="countryCode">The user's country code</param>
        /// <param name="password">The user's password</param>
        /// <param name="comfirmPassword">The user's password confirmation</param>
        /// <param name="pot">Bot buster</param>
        /// <returns>HTTP Code and Message</returns>
        [HttpPost("User/Register")]
        public async System.Threading.Tasks.Task<IActionResult> RegistrationAsync(
            string firstName,
            string lastName,
            string email,
            string countryCode,
            string password,
            string confirmPassword,
            string pot,
            IFormFile image)
        {
            if (pot != "honey")
            {
                return Forbid();
            }

            if (password != confirmPassword)
            {
                return Unauthorized("Password and Confirm Password do not match");
            }

            if (String.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Password is required");
            }

            try
            {
                byte[] imageByte = Utilities.Images.ConvertUploadedImage(image);

                UserModel newUser = this.BuildUserObject(
                    firstName,
                    lastName,
                    email,
                    countryCode,
                    password,
                    imageByte);

                this.userDal.AddUser(newUser);
                
                await this.BuildAuthenticationCookieAsync(newUser);

                if (imageByte != null)
                {
                    this.PutAvatarInSession(imageByte);
                }
            }
            catch (ArgumentException badArugment)
            {
                return BadRequest(badArugment.Message);
            }
            catch (MySqlException mySqlEx)
            {
                if (mySqlEx.Message.IndexOf("email") > 0)
                {
                    return BadRequest("That email already exists");
                }
                throw new Exception(mySqlEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong with your login. Please try again");
            }

            return new StatusCodeResult(200);
        }

        private UserModel BuildUserObject(
            string firstName,
            string lastName,
            string email,
            string countryCode,
            string password,
            byte[] imageByte)
        {
            UserModel newUser = new UserModel
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                CountryCode = countryCode,
                Password = password,
                Image = imageByte
            };

            ValidationContext validationContext = new ValidationContext(newUser);
            List<ValidationResult> validationResults = new List<ValidationResult>();
            if (!Validator.TryValidateObject(newUser, validationContext, validationResults, true))
            {
                String errorMessgae = "";

                foreach (var issue in validationResults)
                {
                    errorMessgae += issue + Environment.NewLine;
                }

                throw new ArgumentException(errorMessgae);
            }

            return newUser;
        }

        /// <summary>
        /// For logging out of The Kitchen
        /// </summary>
        /// <returns>Home Screen</returns>
        [HttpGet("Logout")]
        public async System.Threading.Tasks.Task<RedirectResult> LogoutAsync()
        {
            await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);

            return Redirect("/Home");
        }

        private async System.Threading.Tasks.Task BuildAuthenticationCookieAsync(UserModel user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.GivenName, user.FirstName),
                new Claim(ClaimTypes.Surname, user.LastName),
                new Claim(ClaimTypes.Country, user.CountryCode)
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                IssuedUtc = DateTimeOffset.UtcNow,
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);
        }

        private void PutAvatarInSession(byte[] image)
        {
            HttpContext.Session.Set("avatar", image);
        }

        [Authorize]
        [HttpGet("User/Recipe/TasteRating/{recipeID}")]
        public IActionResult GetRecipeTasteRating(int recipeID)
        {
            int rating = this.userDal.GetUserRecipeTasteRating(
                User.FindFirst(ClaimTypes.Email).Value, recipeID);
            return Ok(rating);
        }

        [Authorize]
        [HttpGet("User/Recipe/DifficultyRating/{recipeID}")]
        public IActionResult GetRecipeDifficultyRating(int recipeID)
        {
            int rating = this.userDal.GetUserRecipeDifficultyRating(
                User.FindFirst(ClaimTypes.Email).Value, recipeID);
            return Ok(rating);
        }
    }
}