﻿using System;
using System.Diagnostics;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchen.Controllers
{
    public class RecipeBookController : Controller
    {
        private readonly RecipeBookDAL recipeBookDAL;

        /// <summary>
        /// RecipeBookController constructor
        /// </summary>
        public RecipeBookController()
        {
            this.recipeBookDAL = new RecipeBookDAL();
        }

        [HttpGet("RecipeBook/Details")]
        public IActionResult Details(int id)
        {
            try
            {
                RecipeBook recipeBook = recipeBookDAL.GetRecipeBook(id);

                TempData["RecipeBook"] = JsonConvert.SerializeObject(recipeBook);
                return new RedirectToActionResult("_RecipeBookDetails", "Account", recipeBook);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Creates a new RecipeBook
        /// </summary>
        /// <param name="recipeBook">The RecipeBook to create</param>
        /// <returns>Redirect back to My Recipe Books tab of account</returns>
        [HttpPost("RecipeBook/Create")]
        public IActionResult Create(RecipeBook recipeBook)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userID = TheKitchen.Utilities.User.GetUserIDFromEmail(User.FindFirst(ClaimTypes.Email).Value);
                    recipeBookDAL.CreateRecipeBook(userID, recipeBook.Name);
                    return new RedirectToActionResult("Account", "Account", new { tab = "recipe-books-tab" });
                }
                else
                {
                    return Redirect("/Account#recipe-books");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Adds a recipe to a specific recipe book
        /// </summary>
        /// <param name="bookID">The ID of the recipe book</param>
        /// <param name="recipeID">The ID of the recipe to be added</param>
        /// <returns>JSON of success or failure</returns>
        [Authorize]
        [HttpPost("RecipeBook/AddRecipe")]
        public IActionResult AddRecipeBookEntryToRecipeBook(int bookID, int recipeID)
        {
            try
            {
                if (bookID <= 0)
                {
                    return StatusCode(500, "Invalid RecipeBook ID.");
                }

                RecipeDAL recipeDAL = new RecipeDAL();
                Recipe recipe = recipeDAL.GetRecipe(recipeID);

                if (!recipeBookDAL.CheckForDuplicateRecipesInBook(bookID, recipe))
                {
                    this.recipeBookDAL.AddRecipeBookEntryToRecipeBook(bookID, recipe);
                    return Json(new { success = true });
                }
                else
                {
                    return Json(new { success = false });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Deletes a Recipe Book Entry from a recipe book
        /// </summary>
        /// <param name="bookEntryID">The ID of the recipe book entry to be removed</param>
        /// <returns>JSON of success or failure</returns>
        [Authorize]
        [HttpPost("RecipeBook/DeleteRecipeFromBook")]
        public IActionResult DeleteRecipeBookEntryFromBook(int bookEntryID)
        {
            try
            {
                if (bookEntryID <= 0)
                {
                    return StatusCode(500, "Invalid RecipeBookEntry ID.");
                }

                recipeBookDAL.DeleteRecipeFromRecipeBook(bookEntryID);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Deletes a Recipe Book and its Entries
        /// </summary>
        /// <param name="bookID">The ID of the recipe book to be removed</param>
        /// <returns>JSON of success or failure</returns>
        [Authorize]
        [HttpPost("RecipeBook/DeleteBook")]
        public IActionResult DeleteRecipeBook(int bookID)
        {
            try
            {
                Debug.WriteLine(bookID);
                if (bookID <= 0)
                {
                    return StatusCode(500, "Invalid RecipeBookEntry ID.");
                }

                recipeBookDAL.DeleteRecipeBook(bookID);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }
    }
}
