﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using TheKitchen.Models;
using TheKitchen.DAL;
using System.Collections.Generic;
using System.Security.Claims;
using System;

namespace TheKitchen.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Endpoint for the Home page
        /// </summary>
        /// <param name="sortBy">Optional sort by parameter</param>
        /// <param name="orderBy">Optional order by parameter</param>
        /// <param name="page">Optional page parameter</param>
        /// <param name="searchPhrase">Optional search phrase parameter</param>
        /// <returns></returns>
        [HttpGet("")]
        [HttpGet("Home")]
        public IActionResult Index(
            string sortBy = "",
            string orderBy = "",
            int page = 1,
            string search = "")
        {
            try
            {
                if (User != null && User.Identity.IsAuthenticated)
                {
                    UserDAL userDAL = new UserDAL();
                    UserModel user = userDAL.GetUser(User.FindFirst(ClaimTypes.Email).Value);
                    ViewData["User"] = user;

                    RecipeBookDAL recipeBookDAL = new RecipeBookDAL();
                    List<RecipeBook> recipeBooks = recipeBookDAL.GetRecipeBooksForUser(user.UserId);
                    ViewData["recipeBooks"] = recipeBooks;
                }

                RecipeDAL recipeDAL = new RecipeDAL();
                List<Recipe> recipes = new List<Recipe>();

                ViewData["page"] = page;
                ViewData["orderBy"] = orderBy;
                ViewData["sortBy"] = sortBy;
                ViewData["search"] = search;
                ViewData["recipeCount"] = recipeDAL.GetTotalRecipeCount();

                string orderByClause = TheKitchen.Utilities.Recipe.BuildRecipeOrderBy(sortBy, orderBy);
                ViewData["pageSize"] = TheKitchen.Utilities.Recipe.pageSize;
                page = TheKitchen.Utilities.Recipe.SetPage(page);

                recipes = recipeDAL.GetAllRecipes(orderByClause, page,
                    TheKitchen.Utilities.Recipe.pageSize, search);
                ViewData["recipes"] = recipes;

                return View("Index");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet("GroceryList")]
        public IActionResult GroceryList() {
            try
            {
                return View("PrintGroceryList");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }
    }
}
