﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TheKitchen.Models;
using TheKitchen.DAL;
using System.Security.Claims;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;

namespace TheKitchen.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        // GET: /<controller>/
        [HttpGet("Account")]
        public IActionResult Account(string tab = "")
        {
            try
            {
                UserDAL userDAL = new UserDAL();
                UserModel user = userDAL.GetUser(User.FindFirst(ClaimTypes.Email).Value);

                RecipeDAL recipeDAL = new RecipeDAL();
                List<Recipe> recipes = recipeDAL.GetAllRecipes(user.UserId);

                RecipeBookDAL recipeBookDAL = new RecipeBookDAL();
                List<RecipeBook> recipeBooks = recipeBookDAL.GetRecipeBooksForUser(user.UserId);

                AccountModel account = new AccountModel();
                account.User = user;
                account.Recipes = recipes;
                account.RecipeBooks = recipeBooks;

                if (tab != "")
                {
                    ViewBag.ActiveTab = tab;
                }

                ViewData["Account"] = account;

                return View("Account");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
           
        }

        [HttpGet("Account/_RecipeBookDetails")]
        public IActionResult _RecipeBookDetails()
        {
            try
            {
                var storedBook = TempData["RecipeBook"].ToString();
                var book = JsonConvert.DeserializeObject<RecipeBook>(storedBook);

                return PartialView("_RecipeBookDetails", book);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }
    }
}
