using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchen.Controllers
{
    /// <summary>
    /// The RecipeController class
    /// </summary>
    public class RecipeController: Controller
    {
        private readonly RecipeDAL recipeData;
        private readonly UserDAL userData;

        /// <summary>
        /// RecipeController constructor
        /// </summary>
        public RecipeController()
        {
            this.recipeData = new RecipeDAL();
            this.userData = new UserDAL();
        }

        /// <summary>
        /// Returns user to recipe index page
        /// </summary>
        /// <returns></returns>
        [HttpGet("Recipe/Index")]
        public ActionResult Index()
        {
            try
            {
                return View("Index",recipeData.GetAllRecipes());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Returns user to a specific recipe page
        /// </summary>
        /// <param name="id">recipe ID to view</param>
        [HttpGet("Recipe/Details/{id}")]
        public ActionResult Details(int id)
        {
            try
            {
                Recipe recipe = recipeData.GetRecipe(id);
                if (recipe != null)
                {
                    if (User != null && User.Identity.IsAuthenticated)
                    {
                        var userEmail = User.FindFirst(ClaimTypes.Email).Value;
                        var user = userData.GetUser(userEmail);

                        RecipeBookDAL recipeBookDAL = new RecipeBookDAL();
                        List<RecipeBook> recipeBooks = recipeBookDAL.GetRecipeBooksForUser(user.UserId);
                        ViewData["recipeBooks"] = recipeBooks;
                    }
                    return View("Details", recipe);
                }
                else
                {
                    return new Utilities.NotFoundViewResult("NotFound");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Used for upserting a given user and recipe
        /// taste rating.
        /// </summary>
        /// <param name="id">The ID of the recipe being rated</param>
        /// <param name="rating">A rating of 1-5</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("Recipe/Rating/Taste")]
        public ActionResult TasteRating(
            int id,
            int rating)
        {
            try
            {
                int userID = userData.GetUser(User.FindFirst(ClaimTypes.Email).Value).UserId;
                if (recipeData.GetRecipe(id).UserId == userID)
                {
                    return Unauthorized("You cannot rate your own recipe");
                }

                recipeData.UpsertTasteRating(id, rating, userID);
                Recipe recipe = recipeData.GetRecipe(id);
                return View("Details", recipe);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Used for upserting a given user and recipe
        /// difficulty rating.
        /// </summary>
        /// <param name="id">The ID of the recipe being rated</param>
        /// <param name="rating">A rating of 1-5</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("Recipe/Rating/Difficulty")]
        public ActionResult DifficultyRating(
            int id,
            int rating)
        {
            try
            {
                int userID = userData.GetUser(User.FindFirst(ClaimTypes.Email).Value).UserId;
                if (recipeData.GetRecipe(id).UserId == userID)
                {
                    return Unauthorized("You cannot rate your own recipe");
                }

                recipeData.UpsertDifficultyRating(id, rating, userID);
                Recipe recipe = recipeData.GetRecipe(id);
                return View("Details", recipe);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Returns customer to new recipe page
        /// </summary>
        [HttpGet("Recipe/Create")]
        public ActionResult Create()
        {
            return View("Create");
        }

        /// <summary>
        /// Creates a new recipe
        /// </summary>
        /// <param name="recipe">Recipe model object to create</param>
        /// <param name="image">The new recipe image</param>
        [Authorize]
        [HttpPost("Recipe/Create")]
        public ActionResult Create(Recipe recipe, IFormFile image)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (image != null)
                    {
                        byte[] imageByte = Utilities.Images.ConvertUploadedImage(image);
                        recipe.Picture = imageByte;
                    }
                    var userEmail = User.FindFirst(ClaimTypes.Email).Value;
                    var user = userData.GetUser(userEmail);

                    var recipeID = this.recipeData.CreateRecipe(recipe, user.UserId, DateTime.Now);
                    return RedirectToAction("Details", new { id = recipeID });
                }
                else
                    return View("Create", recipe);
            }
            catch (MySqlException mySqlEx)
            {
                Console.WriteLine(mySqlEx);
                return StatusCode(500, "Something went wrong. Please try again");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Returns customer to edit recipe page
        /// </summary>
        [Authorize]
        [HttpGet("Recipe/Edit")]
        public IActionResult Edit(int recipeID)
        {
            Recipe recipe = recipeData.GetRecipe(recipeID);
            return View("Edit", recipe);
        }

        [Authorize]
        [HttpPost("Recipe/Edit")]
        public IActionResult Edit(Recipe newRecipe, IFormFile image, int recipeID)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Recipe oldRecipe = recipeData.GetRecipe(recipeID);
                    if (image != null)
                    {
                        byte[] imageByte = Utilities.Images.ConvertUploadedImage(image);
                        newRecipe.Picture = imageByte;
                    }
                    var success = this.recipeData.UpdateRecipe(oldRecipe, newRecipe);
                    return RedirectToAction("Details", new { id = recipeID });
                }
                else
                    return View("Edit", newRecipe);
            }
            catch (MySqlException mySqlEx)
            {
                Console.WriteLine(mySqlEx);
                return StatusCode(500, "Something went wrong. Please try again");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        /// <summary>
        /// Provides real-time autocomplete functionality for ingredient searching on new recipe page
        /// </summary>
        [Produces("application/json")]
        [HttpGet("Recipe/IngredientSearch")]
        public IActionResult IngredientSearch(string searchTerm)
        {
            try
            {
                IngredientDAL ingredientDAL = new IngredientDAL();

                if (String.IsNullOrEmpty(searchTerm))
                {
                    searchTerm = HttpContext.Request.Query["term"].ToString();
                }

                List<Ingredient> ingredients = ingredientDAL.GetAllIngredients().Where(
                    i => i.Name.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)).ToList();

                var matchedIngredients = ingredients.Select(i =>
                    new { label = i.Name, Value = i.Name }).ToList();

                return Json(matchedIngredients);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }

        [Authorize]
        [HttpPost("Recipe/Delete")]
        public IActionResult DeleteRecipe(int id)
        {
            try
            {
                if(id <= 0)
                {
                    return StatusCode(500, "Invalid Recipe ID.");
                }

                this.recipeData.DeleteRecipe(id);
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return StatusCode(500, "Something went wrong. Please try again");
            }
        }
    }
}