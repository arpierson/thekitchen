﻿function addRecipeToBook(frm, e) {
    e.preventDefault();
    var modalID = $(frm).data('recipe-id');
    try {
        $.ajax({
            method: 'POST',
            url: frm.action,
            data: new FormData(frm),
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.success) {
                    $('#add-recipe-btn-' + modalID).html("&#10004 &nbsp; Recipe Added!");
                    setTimeout(function () {
                        $('#add-recipe-btn-' + modalID).html("Add to recipe book")
                    }, 5000);
                } else {
                    $('#add-recipe-btn-' + modalID).html("&#10060 &nbsp; Already added");
                    setTimeout(function () {
                        $('#add-recipe-btn-' + modalID).html("Add to recipe book")
                    }, 5000);
                }
            }
        })
    }
    catch (err) {
        alert(err);
    }
}
