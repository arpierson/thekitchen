﻿/* Variables */
var nameList = JSON.parse(sessionStorage.getItem("shopArray") || "[]");
var ingredientList = JSON.parse(sessionStorage.getItem("ingredientArray") || "[]");
var index;

/* Call Functions */
updateCartNumber();
populateTable();
populateGroceryList();

/* Update Shopping cart value */
function updateCartNumber() {
    $(document).ready(function () {
        if (nameList.length > 0) {
            $(".fa-list").append("<div class=\"badge\">" + nameList.length + "</div>")
        }
        else {
            $('.badge').remove();
        }
    });
}

/* Get Model */
function getModel(string) {
    var stringArray = string.split("  ");
    addNameToList(stringArray[0]);
    addIngredientsToList(string);
}

/* Name List associated functions */
function addNameToList(name) {
    nameList.push(name);
    sessionStorage.setItem('shopArray', JSON.stringify(nameList));
    populateTable();
    updateCartNumber();
    successAlert();
}

/* Remove element from lists */
function removeFromLists(element) {
    index = nameList.indexOf(element);

    nameList.splice(index, 1);
    sessionStorage.setItem('shopArray', JSON.stringify(nameList));

    ingredientList.splice(index, 1);
    sessionStorage.setItem('ingredientArray', JSON.stringify(ingredientList));

    populateTable();
    populateGroceryList();
    updateCartNumber();
}

/* Return name list */
function returnNameList() {
    return JSON.parse(sessionStorage.getItem('shopArray'));
}

/* Add to Ingredient List */
function addIngredientsToList(ingeredients) {
    ingredientList.push(ingeredients);
    sessionStorage.setItem('ingredientArray', JSON.stringify(ingredientList));
}

/* Return ingredient list */
function returnIngredientList() {
    return JSON.parse(sessionStorage.getItem('ingredientArray'));
}

/* Populate shopping cart table */
function populateTable() {
    $('.tr').html('');

    try {
        for (var i = 0, len = returnNameList().length; i < len; i++) {
            $('.tr').append('<tr>');
            $('.tr').append('<td>' + returnNameList()[i] + '</td>');
            $('.tr').append('<td class="kitchen-remove-shopping-list-item">'
                + "<a onclick='" + 'removeFromLists(\"' +
                returnNameList()[i] + '\");' + "'>" + 'Remove' + "</a>" + '</td>');
            $('.tr').append('</tr>');
        }
    } catch {
        return false;
    }
}

/* Populate printable grocery list */
function populateGroceryList() {
    var content = "";
    $('.print-list').html('');

    for (let x = 0; x < ingredientList.length; x++) {
        var array = ingredientList[x].split("     ");
        content += '<strong>' + array[0] + '</strong>' + '<br />';
        for (let y = 1; y < array.length - 1; y++) {
            content += '&emsp;&emsp;';
            content += '<input type="checkbox" style="outline: 1px solid black;width: 15px;height: 15px;display: inline - block;margin - right: 4px;" disabled>';

            content += array[y] + '<br />';
        }
        content += '<br />';
    }

    $('.print-list').append('<p>' + content + '</p>');
}

/* Email list to user */
function sendEmail(email) {
    var emailSubject = "The Kitchen Grocery List";
    var emailBody = 'Here is your list of ingredients:' + '%0D%0A' + '%0D%0A';

    for (let x = 0; x < ingredientList.length; x++) {
        var array = ingredientList[x].split("     ");
        for (let y = 0; y < array.length; y++) {
            emailBody += array[y] + '%0D%0A';
        }
    }

    window.open('mailto:' + email + '?subject=' + emailSubject + '&body=' + emailBody);
}

/* Add to cart Alert */
async function successAlert() {
    $("#success-alert").show();
    await sleep(2000);
    $("#success-alert").hide();
}

/* Sleep */
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/* Clear list upon logging out */
function clearShoppingCart() {
    nameList = [];
    ingredientList = [];

    nameList.length = 0;
    ingredientList.length = 0;

    sessionStorage.setItem('shopArray', JSON.stringify(nameList));
    sessionStorage.setItem('ingredientArray', JSON.stringify(ingredientList));

    $(document).ready(function () {
        $('.basketitems').remove();
    });
}
