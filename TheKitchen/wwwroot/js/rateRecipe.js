﻿// Initializing recipe rating popover
$(document).ready(function () {
    $("#back-button").on("click", function () {
        window.history.back();
    });

    $(function () {
        $('#kitchen-rate-recipe-anchor').popover({
            sanitize: false,
            placement: 'bottom',
            html: true,
            title: 'Your Current Rating',
            content: $('#recipe-rating-form-div').html()
        });
    })

    $("#kitchen-rate-recipe-anchor").on("click", function () {
        getUserRecipeTasteRating();
        getUserRecipeDifficultyRating();
    })
});

//Used to set the user's Taste rating for a given recipe
function tasteRating(id, rating) {
    try {
        $.ajax({
            method: 'POST',
            url: '/Recipe/Rating/Taste',
            data: {
                id: id,
                rating: rating,
            },
        })
            .done(function () {
                window.location.href = window.location.href;
            })
            .fail(function (error) {
                $("#rating-error").text(error.responseText);
            });
    }
    catch (err) {
        alert(err);
    }
}

//Used to set the user's Difficulty rating for a given recipe
function difficultyRating(id, rating) {
    try {
        $.ajax({
            method: 'POST',
            url: '/Recipe/Rating/Difficulty',
            data: {
                id: id,
                rating: rating,
            },
        })
            .done(function () {
                window.location.href = window.location.href;
            })
            .fail(function (error) {
                $("#rating-error").text(error.responseText);
            });
    }
    catch (err) {
        alert(err);
    }
}

//Gets the user's Taste rating
function getUserRecipeTasteRating() {
    try {
        $.ajax({
            method: 'GET',
            url: '/User/Recipe/TasteRating/' + $('#recipeID').val(),
        })
            .done(function (data) {
                setUserTasteRating(data);
            })
            .fail(function (error) {
                $("#rating-error").text(error.responseText);
            });
    }
    catch (err) {
        alert(err);
    }
}

//Sets the number of stars to orange to match the user's rating
function setUserTasteRating(rating) {
    var i = 0;
    $('[id=recipe-taste-rating-div]:eq(1) .kichten-taste-rating-btn').each(function () {
        if (i < rating) {
            $(this).toggleClass('kitchen-rating-starred', true);
        }
        i++;
    });
}

//Gets the user's Difficulty rating
function getUserRecipeDifficultyRating() {
    try {
        $.ajax({
            method: 'GET',
            url: '/User/Recipe/DifficultyRating/' + $('#recipeID').val(),
        })
            .done(function (data) {
                setUserDifficultyRating(data);
            })
            .fail(function (error) {
                $("#rating-error").text(error.responseText);
            });
    }
    catch (err) {
        alert(err);
    }
}

//Sets the number of stars to orange to match the user's rating
function setUserDifficultyRating(rating) {
    var i = 0;
    $('[id=recipe-difficulty-rating-div]:eq(1) .kichten-difficulty-rating-btn').each(function () {
        if (i < rating) {
            $(this).toggleClass('kitchen-rating-starred', true);
        }
        i++;
    });
}