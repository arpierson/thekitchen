﻿// Initialize account tooltip
$(document).ready(function () {
    var urlParams = new URLSearchParams(location.search);
    var search = urlParams.get("search");
    $("#search").val(search);
});