﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var recipeIdToDelete = -1;

$(document).ready(function () {
    $("#delete-recipe").on("click", function () {
        if ($("#recipeID").length > 0) {
            recipeIdToDelete = $('#recipeID').val();
        }
        deleteRecipe();
    });

    $("#my-recipes-delete-btn").on("click", function () {
        recipeIdToDelete = $(this).data("id");
    });
});

function deleteRecipe() {
    try {
        $.ajax({
            method: 'POST',
            url: '/Recipe/Delete',
            data: {
                id:recipeIdToDelete
            },
        })
            .done(function () {
                window.location.href = "/Account/?tab=my-recipes&deleted";
            })
            .fail(function (error) {
                $("#deletion-error").text(error.responseText);
            });
    }
    catch (err) {
        alert(err);
    }
}