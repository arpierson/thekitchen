$(document).ready(function () {
    selectAccountTab();
    showHideDeleteSuccess();
    
    $('#newRecipeBookForm').validate({
        rules: {
            name: {
                required: true,
                maxlength: 45
            }
        },
        messages: {
            name: {
                required: "You must enter a name.",
                maxlength: "Name can be no longer than 45 characters long."
            }
        }
    });

    $("#add-new-recipe").on("click", function () {
        window.location.href = "/Recipe/Create";
    });
});

function saveProfilePic(frm) {
    try {
        var posting = $.post(frm.action, $(frm).serialize());
        posting.done(function () { window.location.href = "/Account"});
        posting.fail(function () { $("#register-error").content = "error"; });
    }
    catch (err) {
        alert(err);
    }
}

/**
 * Sorts an HTML table.
 * 
 * @param {HTMLTableElement} table The table to sort
 * @param {number} column The index of the column to sort
 * @param {boolean} asc Determines if the sorting will be in ascending
 */
function sortTableByColumn(table, column, asc = true) {
    const dirModifier = asc ? 1 : -1;
    const tBody = table.tBodies[0];
    const rows = Array.from(tBody.querySelectorAll("tr"));

    // Sort each row
    const sortedRows = rows.sort((a, b) => {
        const aColText = a.querySelector(`td:nth-child(${column + 1})`).textContent.trim();
        const bColText = b.querySelector(`td:nth-child(${column + 1})`).textContent.trim();

        return aColText > bColText ? (1 * dirModifier) : (-1 * dirModifier);
    });

    // Remove all existing TRs from the table
    while (tBody.firstChild) {
        tBody.removeChild(tBody.firstChild);
    }

    // Re-add the newly sorted rows
    tBody.append(...sortedRows);

    // Remember how the column is currently sorted
    table.querySelectorAll("th").forEach(th => th.classList.remove("th-sort-asc", "th-sort-desc"));
    table.querySelector(`th:nth-child(${column + 1})`).classList.toggle("th-sort-asc", asc);
    table.querySelector(`th:nth-child(${column + 1})`).classList.toggle("th-sort-desc", !asc);
}

document.querySelectorAll(".table-sortable th").forEach(headerCell => {
    headerCell.addEventListener("click", () => {
        const tableElement = headerCell.parentElement.parentElement.parentElement;
        const headerIndex = Array.prototype.indexOf.call(headerCell.parentElement.children, headerCell);
        const currentIsAscending = headerCell.classList.contains("th-sort-asc");

        sortTableByColumn(tableElement, headerIndex, !currentIsAscending);
    });
});

$(".book-title").on("click", function () {
    var recipeBookIdToGet = $(this).data("id");
    var recipeBookName = $(this).data("name");
    getRecipeBook(recipeBookIdToGet, recipeBookName);
});

function getRecipeBook(bookId, bookName) {
    try {
        $.ajax({
            method: 'GET',
            url: '/RecipeBook/Details',
            data: {
                id: bookId
            },
            dataType: "html",
            success: function (response) {
                $('.table-placeholder').html(response);
                $('#bookModal').modal('show');
            }
        });
        $('.modal-book-title').text(bookName);
    }
    catch (err) {
        alert(err);
    }
}

function selectAccountTab() {
    var urlParams = new URLSearchParams(location.search);
    switch (urlParams.get("tab")) {
        case "recipe-books-tab":
            $('.nav-tabs li:eq(1) a').tab('show')
            break;
        case "my-recipes":
            $('.nav-tabs li:eq(2) a').tab('show')
            break;
        default:
            break;
    }
}

async function showHideDeleteSuccess() {
    var urlParams = new URLSearchParams(location.search);

    $('#delete-success').toggleClass('d-none', true);
    if (urlParams.has("deleted")) {
        $('#delete-success').toggleClass('d-none', );

        await sleep(2000);
        $('#delete-success').toggleClass('d-none', true);
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}