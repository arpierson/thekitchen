/* Initialize autocomplete */
$(document).ready(function () {
    autocomplete();
    $("#instructionTable tbody").sortable({
        update: function (event, ui) {
            renumberIndex();
        }
    });
    $('#newRecipeForm').validate({
        rules: {
            title: {
                required: true,
                minlength: 2,
                maxlength: 45
            },
            description: {
                required: true,
                minlength: 2,
                maxlength: 500
            }
        },
        messages: {
            title: {
                required: "You must enter a title.",
                minlength: "Title must be at least 2 characters long.",
                maxlength: "Title can be no longer than 45 characters long."
            },
            description: {
                required: "You must enter a description.",
                minlength: "Description must be at least 2 characters long.",
                maxlength: "Description can be no longer than 500 characters long."
            }
        }
    });
    renumberIndex();
});

$('#addNewIngredient').click(function () {
    addIngredient();
});

$('#addNewInstruction').click(function () {
    addInstruction();
});

/* Script for autocomplete in ingredient search box. */
function autocomplete() {
    $("#ingredientSearch").autocomplete({
        appendTo: ".addIngredients",
        source: function (request, response) {
            $.ajax({
                url: '/Recipe/IngredientSearch',
                type: 'GET',
                cache: false,
                data: request,
                dataType: 'json',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.Value
                        }
                    }))
                }
            });
        }
    });
};

/* Script to add ingredient search bar text to ingredient list if not already present. */
function addIngredient() {
    $('#confirm-empty-ingredient').toggleClass('d-none', true);
    $('#confirm-duplicate-ingredient').toggleClass('d-none', true);

/* var listIndex = $('.recipeIngredientRow').length; */
    var listIndex = $('#ingredientTable >tbody >tr').length;
    var measurement = '<input type="text" asp-for="Ingredients[' + listIndex + '].Measurement" name="Ingredients[' + listIndex +
        '].Measurement" class="measurementTextBox form-control form-control-lg" id="measurementTextBox' + listIndex + '" placeholder="Enter measurement" required>';
    var ingredient = '<input type="text" class="noTextborder" asp-for="Ingredients[' + listIndex + '].Ingredient" name="Ingredients[' + listIndex +
        '].Ingredient" value="' + $('#ingredientSearch').val() + '" readonly>';
    var deleteButton = '<input type="button" class="cancel" value="X">';
    var newRow = '<tr class="recipeIngredientRow"><td>' + measurement + '</td><td>' + ingredient +
        '</td><td>' + deleteButton + '</td></tr>';

    var hasDuplicates = false;
    $("#ingredientTable td").each(function () {
        var measurementContent = $(this).find('input').val().length;

        var ingredientContent = $(this).find('input').val() == $('#ingredientSearch').val();

        if (ingredientContent) {
            hasDuplicates = true;
            return;
        }
    });

    if ($('#ingredientSearch').val().length == 0) {
        $('#confirm-empty-ingredient').toggleClass('d-none', false);
    }
    else if (hasDuplicates) {
        $('#confirm-duplicate-ingredient').toggleClass('d-none', false);
    }
    else {
        $('#confirm-empty-ingredient').toggleClass('d-none', true);
        $('#confirm-duplicate-ingredient').toggleClass('d-none', true);
        $('#confirm-no-ingredient').toggleClass('d-none', true);
        $('#ingredientTable').append(newRow);
        $('#ingredientSearch').val("");
    } 
};

/* Script for removing ingredient table row when 'X' button is clicked. */
$('#ingredientTable').on('click', 'tr input.cancel', function () {
    $(this).closest('tr').remove();
    $('#ingredientTable tr').each(function () {
        $('td:nth-child(1)', this).find('input').attr('name', 'Ingredients[' + $(this).index() + '].Measurement');
        $('td:nth-child(2)', this).find('input').attr('name', 'Ingredients[' + $(this).index() + '].Ingredient');
    });
});

/* Script for adding instruction step to instruction list */
function addInstruction() {
    var listIndex = $('#instructionTable >tbody >tr').length;
    var reorderIcon = '<i class="fa fa-bars"></i>';
    var stepNumber = '<input type="number" readonly="true" asp-for="Directions" name="Directions[' + listIndex +
        '].Step" class="noTextborder stepNumber" value="' + (listIndex + 1) + '">';
    var instruction = '<textarea asp-for="Directions" name="Directions[' + listIndex +
        '].InstructionText" id="Directions[' + listIndex + ']" class="noTextborder" rows="2" cols="50" readonly>' +
        $('#instructionText').val() + '</textarea>';
    var deleteButton = '<input type="button" class="cancel" value="X"';
    var newRow = '<tr class="recipeInstructionRow"><td>' + reorderIcon + '</td> <td class="ten">' + stepNumber +
        ' </td> <td class="eighty">' + instruction + '</td> <td class="ten">' + deleteButton + '</td></tr > ';

    if ($('#instructionText').val().length <= 1) {
        $('#confirm-short-instruction').toggleClass('d-none', false);
    }
    else {
        $('#instructionTable').append(newRow);
        renumberIndex();
        $('#confirm-short-instruction').toggleClass('d-none', true);
        $('#confirm-no-instruction').toggleClass('d-none', true);
        $('#instructionText').val("");
    }
}

/* Script for removing instruction table row when 'X' button is clicked */
$('#instructionTable').on('click', 'tr input.cancel', function () {
    $(this).closest('tr').remove();
    renumberIndex();
});

/* Script for renumbering the Step number in Instruction table when elements change */
function renumberIndex() {
    $('#instructionTable tr').each(function () {
        var rowIndex = $(this).index() + 1;
        $('td:nth-child(2)', this).find('input').val(rowIndex);
        $('td:nth-child(2)', this).find('input').attr('name', 'Directions[' + $(this).index() + '].Step');
        $('td:nth-child(3)', this).find('textarea').attr('name', 'Directions[' + $(this).index() + '].InstructionText');
    });
}

/* Validation for presence of ingredients and instructions */
$("#newRecipeButton").click(function (e) {
    var ingredientCount = $('#ingredientTable tr').length;
    var instructionCount = $('#instructionTable tr').length;

    if ($('#newRecipeForm').valid()) {
        if (ingredientCount == 0) {
            e.preventDefault();
            $('#confirm-no-ingredient').toggleClass('d-none', false);
        }
        if (instructionCount == 0) {
            e.preventDefault();
            $('#confirm-no-instruction').toggleClass('d-none', false);
        }
    }
    else {
        e.preventDefault();
    }
})

/* Fills in file name in file input box */
$("#recipeFileUpload").on("change", function () {
    var fileName = $(this).val().split("\\").pop();
    $("#recipeFileUploadLabel").addClass("selected").html(fileName);
});
