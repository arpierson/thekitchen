﻿$(document).ready(function () {
    //for file uploads
    $("#registration-file-upload").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $("#registration-file-upload-label").addClass("selected").html(fileName);
    });

    //for password validation
    $("#password").on("change", function () {
        $('#invalid-password-div').text('');

        if ($(this).val() == '') {
            $('#confirm-password').change();
            return;
        }

        if (typeof this.reportValidity === 'function') {
            if (!this.checkValidity()) {
                $('#invalid-password-div').text($('#password').attr('title'));
            }
        }
    });

    //for password confirmation
    $("#confirm-password").on("input", function () {
        if ($(this).val() == '') {
            $('#confirm-password-invalid').toggleClass('d-none', true);
            return;
        }

        if ($('#password').val() != ''
            && $('#password').val() != $('#confirm-password').val()
            && $('#confirm-password').val() != '') {
            $('#confirm-password-invalid').toggleClass('d-none', false);
            $('#submit-register').attr('disabled', true);
            $('#submit-register').toggleClass('kitchen-btn-disabled', true);
        } else {
            $('#confirm-password-invalid').toggleClass('d-none', true);
            $('#submit-register').attr('disabled', false);
            $('#submit-register').toggleClass('kitchen-btn-disabled', false);
        }
    });

    $("#submit-update").on("click", function () {
        postForm(this.form, "User/Update");
    });

    $("#submit-register").on("click", function () {
        postForm(this.form, "User/Register");
    });
});

function postForm(frm, action) {
    try {
        $.ajax({
            method: 'POST',
            url: action,
            data: new FormData(frm),
            cache: false,
            contentType: false,
            processData: false,
        })
            .done(function () {
                window.location.href = "/Account"
            })
            .fail(function (error) {
                $("#registration-error").text(error.responseText);
            });
    }
    catch (err) {
        alert(err);
    }
}