﻿var bookIdToDelete = -1;

$(document).ready(function () {
    $("#delete-book").on("click", function () {
        deleteRecipeBook();
    });

    $(".recipe-book-delete-btn").on("click", function () {
        bookIdToDelete = $(this).data("id");
    });
});

function deleteRecipeBook() {
    try {
        $.ajax({
            method: 'POST',
            url: '/RecipeBook/DeleteBook',
            data: {
                bookID: bookIdToDelete
            },
        })
            .done(function () {
                window.location.href = "/Account/?tab=recipe-books-tab&deleted";
            })
            .fail(function (error) {
                $("#deletion-error").text(error.responseText);
            });
    }
    catch (err) {
        alert(err);
    }
}