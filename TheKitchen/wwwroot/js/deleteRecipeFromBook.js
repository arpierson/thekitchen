﻿var recipeIdToDelete = -1;

$(document).ready(function () {
    $(".recipe-book-detail-delete-btn").on("click", function () {
        recipeIdToDelete = $(this).data("id");
        deleteRecipeFromBook();
    });
});

function deleteRecipeFromBook() {
    try {
        $.ajax({
            method: 'POST',
            url: '/RecipeBook/DeleteRecipeFromBook',
            data: {
                bookEntryID: recipeIdToDelete
            },
        })
            .done(function () {
                window.location.href = "/Account/?tab=recipe-books-tab&deleted";
            })
            .fail(function (error) {
                $("#deletion-error").text(error.responseText);
            });
    }
    catch (err) {
        alert(err);
    }
}