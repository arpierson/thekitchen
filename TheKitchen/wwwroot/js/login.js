﻿// Initializing lgin popover
$(function () {
    $('#kitchen-sign-in-anchor').popover({
        sanitize: false,
        placement: 'bottom',
        title: 'Welcome Back!',
        html: true,
        content: $('#login-form-div').html()
    });
})

function login(frm) {
    try {
        $('#login-button').click(function () {
            var btn = $(this);
            btn.val(btn.data("loading-text")); setTimeout(function () {
                btn.val('reset');
            }, 2000);
        });

        $("[id=login-error]:eq(1)").text("");

        var posting = $.post(frm.action, $(frm).serialize());
        posting.done(function () { window.location.href = window.location.href});
        posting.fail(function (error) {
            $("[id=login-error]:eq(1)").text(error.responseText);
        });
    }
    catch (err) {
        alert(err);
    }
}