﻿// Initialize account tooltip
$(document).ready(function () {
    $("#order-by-select").on("change", function () {
        $("#orderBy").val($(this).val());
        $("#sortBy").val($("#sort-by-select").val());
        $("#search-form").submit();
    });

    $("#sort-by-select").on("change", function () {
        $("#sortBy").val($(this).val());
        $("#orderBy").val($("#order-by-select").val());
        $("#search-form").submit();
    });

    $("#order-by-select").val($("#selectedOrderBy").val());
    $("#sort-by-select").val($("#selectedSortBy").val());

    $(".kitchen-page-div").on("click", function () {
        $("#page").val($(this).text());
        $("#recipe-filter").submit();
    });

    setOrderSortByValues();
});

function setOrderSortByValues() {
    var urlParams = new URLSearchParams(location.search);
    var sortBy = urlParams.get("sortBy");
    var orderBy = urlParams.get("orderBy");

    $("#sort-by-select").val(sortBy);
    $("#sortBy").val(sortBy);

    $("#order-by-select").val(orderBy);
    $("#orderBy").val(orderBy);
}