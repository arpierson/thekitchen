using System;
using System.Collections.Generic;
using System.Data;
using MySqlConnector;
using TheKitchen.Models;

namespace TheKitchen.DAL
{
    /// <summary>
    /// The RecipeDAL class
    /// </summary>
    public class RecipeDAL
    {
        private const int pageSize = 1000000;

        /// <summary>
        /// Retrieves a List of All Recipes
        /// </summary>
        /// <returns>List<Recipe></returns>
        public List<Recipe> GetAllRecipes() {
            return this.GetAllRecipes(String.Empty, 0, pageSize, -1, String.Empty);
        }

        /// <summary>
        /// Retrieves List of All Recipes, one page at a time,
        /// ordered as specified, and limited by the rowCount
        /// </summary>
        /// <param name="orderByClause">The recipe property to order by</param>
        /// <param name="page">The page of recipes to retrieve</param>
        /// <param name="pageSize">The number of pages per page</param>
        /// <param name="searchPhrase">The phrase used to search recipes</param>
        /// <returns>List<Recipe></returns>
        public List<Recipe> GetAllRecipes(
            string orderByClause,
            int page,
            int pageSize,
            string searchPhrase) {

            return this.GetAllRecipes(orderByClause, page, pageSize, -1, searchPhrase);
        }

        /// <summary>
        /// Retrieves List of a user's Recipes
        /// </summary>
        /// <param name="userID">The user's ID</param>
        /// <returns>List<Recipe></returns>
        public List<Recipe> GetAllRecipes(int userID) {
            // setting rowCount to a high number so we do not need
            // to worry about paging on the My Recipe tab
            return this.GetAllRecipes(String.Empty, 0, pageSize, userID, String.Empty);
        }

        private List<Recipe> GetAllRecipes(
            string orderByClause,
            int page,
            int pageSize,
            int userID,
            string searchPhrase)
        {
            List<Recipe> recipes = new List<Recipe>();

            string selectCommand = @"
                SELECT
                    *
                FROM
                    (
                        SELECT
                            r.*
                            ,IFNULL(
                                ROUND(
                                    ((
                                        SELECT
                                            SUM(rating)
                                        FROM
                                            Taste_Rating
                                        WHERE
                                            dateDeleted IS NULL
                                            AND recipeid = r.id
                                        ) / (
                                        SELECT
                                            COUNT(rating)
                                        FROM
                                            Taste_Rating
                                        WHERE
                                            dateDeleted IS NULL
                                            AND recipeid = r.id
                                        )
                                        ), 0), 0) AS tasteRating
                            ,IFNULL(
                                ROUND(
                                    ((
                                        SELECT
                                            SUM(rating)
                                        FROM
                                            Difficulty_Rating
                                        WHERE
                                            dateDeleted IS NULL
                                            AND recipeid = r.id
                                        ) / (
                                        SELECT
                                            COUNT(rating)
                                        FROM
                                            Difficulty_Rating
                                        WHERE
                                            dateDeleted IS NULL
                                            AND recipeid = r.id
                                        )
                                        ), 0), 0) AS difficultyRating
                        FROM
                            Recipe AS r
                        WHERE
                            r.dateDeleted IS NULL";

            if (userID != -1) {
                selectCommand += " AND r.userId = @userID";
            }

            if (!String.IsNullOrEmpty(searchPhrase))
            {
                selectCommand += @" AND (
                    r.`name` LIKE @searchPhrase
	                OR
	                r.description LIKE @searchPhrase
	                OR
                    r.id IN (
			            SELECT 
				            recipeID 
			            FROM 
				            Recipe_Ingredient ri INNER JOIN Ingredient i ON ri.ingredientId = i.id
				        WHERE i.`name` LIKE @searchPhrase))";
            }

            selectCommand += @"
                ) recipes" +
                    orderByClause +
                    " LIMIT " + page.ToString() + "," + pageSize.ToString();

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand(selectCommand, connection);

                command.Parameters.AddWithValue("@userID", userID);
                command.Parameters.AddWithValue("@searchPhrase", '%' + searchPhrase + '%');

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        byte[] picture = null;
                        if (reader["picture"] != null & reader["picture"] != DBNull.Value)
                        {
                            picture = (byte[])reader["picture"];
                        }

                        recipes.Add(new Recipe()
                        {
                            UserId = Convert.ToInt32(reader["userId"]),
                            Title = reader["name"].ToString(),
                            Description = reader["description"].ToString(),
                            Id = Convert.ToInt32(reader["id"]),
                            Picture = picture,
                            DateAdded = (DateTime)reader["dateAdded"],
                            DifficultyRating = Convert.ToInt32(reader["difficultyRating"]),
                            TasteRating = Convert.ToInt32(reader["tasteRating"])
                        });
                    }
                }
            }
            return recipes;
        }

        /// <summary>
        /// Builds and returns a specific recipe
        /// </summary>
        /// <param name="recipeID">The id of the recipe to be retrieved</param>
        /// <returns>A recipe object from the database</returns>
        public Recipe GetRecipe(int recipeID)
        {
            Recipe recipe = new Recipe();
            List<RecipeIngredient> ingredients = new List<RecipeIngredient>();
            List<Instruction> instructions = new List<Instruction>();

            string getRecipeString = @"
                SELECT
                    r.*
                    ,IFNULL(
                        ROUND(
                            ((
                                SELECT
                                    SUM(rating)
                                FROM
                                    Taste_Rating
                                WHERE
                                    dateDeleted IS NULL
                                    AND recipeid = r.id
                              ) / (
                                SELECT
                                    COUNT(rating)
                                FROM
                                    Taste_Rating
                                WHERE
                                    dateDeleted IS NULL
                                    AND recipeid = r.id
                                )
                              ), 0), 0) AS tasteRating
                    , IFNULL(
                        ROUND(
                            ((
                                SELECT
                                    SUM(rating)
                                FROM
                                    Difficulty_Rating
                                WHERE
                                    dateDeleted IS NULL
                                    AND recipeid = r.id
                              ) / (
                                SELECT
                                    COUNT(rating)
                                FROM
                                    Difficulty_Rating
                                WHERE
                                    dateDeleted IS NULL
                                    AND recipeid = r.id
                                )
                              ), 0), 0) AS difficultyRating
                FROM
                    Recipe AS r
                WHERE
                    id=@id
                AND dateDeleted IS NULL";

            string getRecipeIngredientString = @"
                SELECT * FROM Recipe_Ingredient WHERE recipeId=@recipeID AND dateDeleted IS NULL";
            string getIngredientString = @"
                SELECT * FROM Ingredient WHERE id=@id AND dateDeleted IS NULL";
            string getInstructionString = @"
                   SELECT * FROM Instruction WHERE recipeId=@recipeID
                        AND dateDeleted IS NULL ORDER BY step";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                MySqlCommand getRecipeCommand = new MySqlCommand(getRecipeString, connection);
                MySqlCommand getRecipeIngredientCommand = new MySqlCommand(getRecipeIngredientString, connection);
                MySqlCommand getIngredientCommand = new MySqlCommand(getIngredientString, connection);
                MySqlCommand getInstructionCommand = new MySqlCommand(getInstructionString, connection);

                getRecipeCommand.Parameters.AddWithValue("@id", recipeID);
                using (var reader = getRecipeCommand.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        byte[] picture = null;
                        if (reader["picture"] != null & reader["picture"] != DBNull.Value)
                        {
                            picture = (byte[])reader["picture"];
                        }

                        recipe.UserId = Convert.ToInt32(reader["userId"]);
                        recipe.Id = Convert.ToInt32(reader["id"]);
                        recipe.Title = reader["name"].ToString();
                        recipe.Description = reader["description"].ToString();
                        recipe.Picture = picture;
                        recipe.DateAdded = (DateTime)reader["dateAdded"];
                        recipe.DifficultyRating = Convert.ToInt32(reader["difficultyRating"]);
                        recipe.TasteRating = Convert.ToInt32(reader["tasteRating"]);
                    }
                }
                if (String.IsNullOrEmpty(recipe.Title))
                {
                    recipe = null;
                    return recipe;
                }

                getRecipeIngredientCommand.Parameters.AddWithValue("@recipeID", recipeID);
                using (var reader = getRecipeIngredientCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        RecipeIngredient current = new RecipeIngredient
                        {
                            Id = Int32.Parse(reader["id"].ToString()),
                            IngredientID = Int32.Parse(reader["ingredientId"].ToString()),
                            Measurement = reader["measurement"].ToString(),
                            DateAdded = (DateTime)reader["dateAdded"]
                        };
                        ingredients.Add(current);
                    }
                }
                foreach (RecipeIngredient r in ingredients)
                {
                    getIngredientCommand.Parameters.Clear();
                    getIngredientCommand.Parameters.AddWithValue("@id", r.IngredientID);
                    using (var reader = getIngredientCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            r.Ingredient = reader["name"].ToString();
                        }
                    }
                }
                getInstructionCommand.Parameters.AddWithValue("@recipeID", recipeID);
                using (var reader = getInstructionCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Instruction current = new Instruction();
                        current.Step = Int32.Parse(reader["step"].ToString());
                        current.InstructionText = reader["instruction"].ToString();
                        current.Id = Int32.Parse(reader["id"].ToString());
                        current.DateAdded = (DateTime)reader["dateAdded"];
                        instructions.Add(current);
                    }
                }
            }
            recipe.Ingredients = ingredients;
            recipe.Directions = instructions;
            return recipe;
        }

        /// <summary>
        /// Creates a new recipe, recipe ingredients, and recipe instructions
        /// </summary>
        /// <param name="recipe">The recipe object to create</param>
        /// <param name="user">The user creating the recipe</param>
        /// <param name="dateAdded">The date of creation</param>
        /// <returns>boolean value indicating recipe creation success</returns>
        public int CreateRecipe(Recipe recipe, int userID, DateTime dateAdded)
        {
            IngredientDAL ingredientDAL = new IngredientDAL();
            bool addedIngredients = CheckForExistingIngredients(recipe.Ingredients, userID, dateAdded);

            if (!addedIngredients)
            {
                return 0;
            }
            else
            {
                MySqlTransaction sqlTransaction;
                using (MySqlConnection connection = Connection.GetConnection())
                {
                    connection.Open();
                    sqlTransaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);

                    string createRecipeCommandString = @"
                        INSERT INTO Recipe (userId, name, description, picture, dateAdded)
                        VALUES (@userID, @name, @description, @picture, @dateAdded)";

                    string recipeIDCommandString = @"
                        SELECT id FROM Recipe WHERE name=@name AND userId=@userID";

                    string createRecipeIngredientString = @"
                        INSERT INTO Recipe_Ingredient (ingredientId, recipeId, measurement, dateAdded)
                        VALUES (@ingredientID, @recipeID, @measurement, @dateAdded)";

                    string createInstructionString = @"
                        INSERT INTO Instruction (recipeId, step, instruction, dateAdded)
                        VALUES (@recipeID, @step, @instruction, @dateAdded)";

                    using (MySqlCommand createRecipeCommand = new MySqlCommand(createRecipeCommandString, connection, sqlTransaction),
                        getRecipeIDCommand = new MySqlCommand(recipeIDCommandString, connection, sqlTransaction),
                        createRecipeIngredientCommand = new MySqlCommand(createRecipeIngredientString, connection, sqlTransaction),
                        createInstructionCommand = new MySqlCommand(createInstructionString, connection, sqlTransaction))
                    {
                        createRecipeCommand.Parameters.AddWithValue("@userID", userID);
                        createRecipeCommand.Parameters.AddWithValue("@name", recipe.Title);
                        createRecipeCommand.Parameters.AddWithValue("@description", recipe.Description);
                        createRecipeCommand.Parameters.AddWithValue("@picture", recipe.Picture);
                        createRecipeCommand.Parameters.AddWithValue("@dateAdded", dateAdded);

                        int createRecipeResult = createRecipeCommand.ExecuteNonQuery();
                        if (createRecipeResult > 0)
                        {
                            int recipeID = 0;
                            getRecipeIDCommand.Parameters.AddWithValue("@name", recipe.Title);
                            getRecipeIDCommand.Parameters.AddWithValue("@userID", userID);
                            using (MySqlDataReader reader = getRecipeIDCommand.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    if (!int.TryParse(reader["id"].ToString(), out recipeID))
                                    {
                                        sqlTransaction.Rollback();
                                        return 0;
                                    }
                                }
                            }

                            foreach (RecipeIngredient r in recipe.Ingredients)
                            {
                                int ingredientID = ingredientDAL.GetIngredientID(r);
                                createRecipeIngredientCommand.Parameters.Clear();
                                createRecipeIngredientCommand.Parameters.AddWithValue("@ingredientID", ingredientID);
                                createRecipeIngredientCommand.Parameters.AddWithValue("@recipeID", recipeID);
                                createRecipeIngredientCommand.Parameters.AddWithValue("@measurement", r.Measurement);
                                createRecipeIngredientCommand.Parameters.AddWithValue("@dateAdded", dateAdded);

                                int createRecipeIngredientResult = createRecipeIngredientCommand.ExecuteNonQuery();
                                if (createRecipeIngredientResult == 0)
                                {
                                    sqlTransaction.Rollback();
                                    return 0;
                                }
                            }

                            foreach (Instruction i in recipe.Directions)
                            {
                                createInstructionCommand.Parameters.Clear();
                                createInstructionCommand.Parameters.AddWithValue("@recipeID", recipeID);
                                createInstructionCommand.Parameters.AddWithValue("@step", i.Step);
                                createInstructionCommand.Parameters.AddWithValue("@instruction", i.InstructionText);
                                createInstructionCommand.Parameters.AddWithValue("@dateAdded", dateAdded);

                                int createInstructionResult = createInstructionCommand.ExecuteNonQuery();
                                if (createInstructionResult == 0)
                                {
                                    sqlTransaction.Rollback();
                                    return 0;
                                }
                            }
                            sqlTransaction.Commit();
                            return GetLastRecipeID();
                        }
                        else
                        {
                            sqlTransaction.Rollback();
                            return 0;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates an existing recipe
        /// </summary>
        /// <param name="oldRecipe">The recipe to be updated</param>
        /// <param name="newRecipe">The updated recipe</param>
        /// <returns>True or false reflecting success of the update statement</returns>
        public bool UpdateRecipe(Recipe oldRecipe, Recipe newRecipe)
        {
            IngredientDAL ingredientDAL = new IngredientDAL();
            bool addedIngredients = CheckForExistingIngredients(newRecipe.Ingredients, oldRecipe.UserId, DateTime.Now);

            if (!addedIngredients)
            {
                return false;
            }
            else
            {
                MySqlTransaction sqlTransaction;
                using (MySqlConnection connection = Connection.GetConnection())
                {
                    connection.Open();
                    sqlTransaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);

                    string updateRecipeStatement = @"
                        UPDATE Recipe SET
                        name = @newName, description = @newDescription, picture = @newPicture, dateModified = Now()
                        WHERE
                        id = @oldID AND userId = @oldUserID AND name = @oldName AND description = @oldDescription AND dateAdded = @oldDateAdded AND dateDeleted IS NULL";

                    string setRecipeIngredientDeleteString = @"
                        UPDATE Recipe_Ingredient SET
                        dateDeleted = Now()
                        WHERE
                        id = @oldRIID AND ingredientId = @oldRIIngredientID AND recipeId = @oldRIRecipeID AND measurement = @oldMeasurement AND dateAdded = @oldRIDateAdded";

                    string createRecipeIngredientString = @"
                        INSERT INTO Recipe_Ingredient (ingredientId, recipeId, measurement, dateAdded)
                        VALUES (@ingredientID, @recipeID, @measurement, Now())";

                    string setInstructionDeleteString = @"
                        UPDATE Instruction SET
                        dateDeleted = Now()
                        WHERE
                        id = @oldIID AND recipeId = @oldIRecipeID AND step = @oldStep AND instruction = @oldInstruction AND dateAdded = @oldIDateAdded";

                    string createInstructionString = @"
                        INSERT INTO Instruction (recipeId, step, instruction, dateAdded)
                        VALUES (@recipeID, @step, @instruction, Now())";

                    using (MySqlCommand updateRecipeCommand = new MySqlCommand(updateRecipeStatement, connection, sqlTransaction),
                        createRecipeIngredientCommand = new MySqlCommand(createRecipeIngredientString, connection, sqlTransaction),
                        setRecipeIngredientDeleteCommand = new MySqlCommand(setRecipeIngredientDeleteString, connection, sqlTransaction),
                        setInstructionDeleteCommand = new MySqlCommand(setInstructionDeleteString, connection, sqlTransaction),
                        createInstructionCommand = new MySqlCommand(createInstructionString, connection, sqlTransaction))
                    {
                        updateRecipeCommand.Parameters.AddWithValue("@oldId", oldRecipe.Id);
                        updateRecipeCommand.Parameters.AddWithValue("@oldUserID", oldRecipe.UserId);
                        updateRecipeCommand.Parameters.AddWithValue("@oldName", oldRecipe.Title);
                        updateRecipeCommand.Parameters.AddWithValue("@oldDescription", oldRecipe.Description);
                        updateRecipeCommand.Parameters.AddWithValue("@oldDateAdded", oldRecipe.DateAdded);

                        updateRecipeCommand.Parameters.AddWithValue("@newName", newRecipe.Title);
                        updateRecipeCommand.Parameters.AddWithValue("@newDescription", newRecipe.Description);
                        if (newRecipe.Picture != null)
                        {
                            updateRecipeCommand.Parameters.AddWithValue("@newPicture", newRecipe.Picture);
                        }
                        else
                        {
                            updateRecipeCommand.Parameters.AddWithValue("@newPicture", oldRecipe.Picture);
                        }

                        int updateRecipeResult = updateRecipeCommand.ExecuteNonQuery();

                        if (updateRecipeResult > 0)
                        {
                            var sameIngredientListLength = oldRecipe.Ingredients.Count == newRecipe.Ingredients.Count;
                            var sameIngredients = true;
                            if (sameIngredientListLength)
                            {
                                for (var i = 0; i < newRecipe.Ingredients.Count; i++)
                                {
                                    if (oldRecipe.Ingredients[i].Ingredient != newRecipe.Ingredients[i].Ingredient ||
                                        oldRecipe.Ingredients[i].Measurement != newRecipe.Ingredients[i].Measurement)
                                    {
                                        sameIngredients = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                sameIngredients = false;
                            }

                            if (!sameIngredients)
                            {
                                foreach (RecipeIngredient r in oldRecipe.Ingredients)
                                {
                                    setRecipeIngredientDeleteCommand.Parameters.Clear();
                                    setRecipeIngredientDeleteCommand.Parameters.AddWithValue("@oldRIID", r.Id);
                                    setRecipeIngredientDeleteCommand.Parameters.AddWithValue("@oldRIIngredientID", r.IngredientID);
                                    setRecipeIngredientDeleteCommand.Parameters.AddWithValue("@oldRIRecipeID", oldRecipe.Id);
                                    setRecipeIngredientDeleteCommand.Parameters.AddWithValue("@oldMeasurement", r.Measurement);
                                    setRecipeIngredientDeleteCommand.Parameters.AddWithValue("@oldRIDateAdded", r.DateAdded);

                                    int deleteRecipeIngredientResult = setRecipeIngredientDeleteCommand.ExecuteNonQuery();
                                    if (deleteRecipeIngredientResult == 0)
                                    {
                                        sqlTransaction.Rollback();
                                        return false;
                                    }
                                }

                                foreach (RecipeIngredient r in newRecipe.Ingredients)
                                {
                                    int ingredientID = ingredientDAL.GetIngredientID(r);
                                    createRecipeIngredientCommand.Parameters.Clear();
                                    createRecipeIngredientCommand.Parameters.AddWithValue("@ingredientID", ingredientID);
                                    createRecipeIngredientCommand.Parameters.AddWithValue("@recipeID", oldRecipe.Id);
                                    createRecipeIngredientCommand.Parameters.AddWithValue("@measurement", r.Measurement);

                                    int createRecipeIngredientResult = createRecipeIngredientCommand.ExecuteNonQuery();
                                    if (createRecipeIngredientResult == 0)
                                    {
                                        sqlTransaction.Rollback();
                                        return false;
                                    }
                                }
                            }

                            var sameInstructionListLength = oldRecipe.Directions.Count == newRecipe.Directions.Count;
                            var sameInstructions = true;
                            if (sameInstructionListLength)
                            {
                                for (var i = 0; i < newRecipe.Directions.Count; i++)
                                {
                                    if (oldRecipe.Directions[i].Step != newRecipe.Directions[i].Step ||
                                        oldRecipe.Directions[i].InstructionText != newRecipe.Directions[i].InstructionText)
                                    {
                                        sameInstructions = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                sameInstructions = false;
                            }

                            if (!sameInstructions)
                            {
                                foreach (Instruction i in oldRecipe.Directions)
                                {
                                    setInstructionDeleteCommand.Parameters.Clear();
                                    setInstructionDeleteCommand.Parameters.AddWithValue("@oldIID", i.Id);
                                    setInstructionDeleteCommand.Parameters.AddWithValue("@oldIRecipeID", oldRecipe.Id);
                                    setInstructionDeleteCommand.Parameters.AddWithValue("@oldStep", i.Step);
                                    setInstructionDeleteCommand.Parameters.AddWithValue("@oldInstruction", i.InstructionText);
                                    setInstructionDeleteCommand.Parameters.AddWithValue("@oldIDateAdded", i.DateAdded);

                                    int deleteInstructionResult = setInstructionDeleteCommand.ExecuteNonQuery();
                                    if (deleteInstructionResult == 0)
                                    {
                                        sqlTransaction.Rollback();
                                        return false;
                                    }
                                }

                                foreach (Instruction i in newRecipe.Directions)
                                {
                                    createInstructionCommand.Parameters.Clear();
                                    createInstructionCommand.Parameters.AddWithValue("@recipeID", oldRecipe.Id);
                                    createInstructionCommand.Parameters.AddWithValue("@step", i.Step);
                                    createInstructionCommand.Parameters.AddWithValue("@instruction", i.InstructionText);

                                    int createInstructionResult = createInstructionCommand.ExecuteNonQuery();
                                    if (createInstructionResult == 0)
                                    {
                                        sqlTransaction.Rollback();
                                        return false;
                                    }
                                }
                            }

                            sqlTransaction.Commit();
                            return true;
                        }
                        else
                        {
                            sqlTransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Used for REPLACING a Taste rating record. If the record is new
        /// and INSERT will be performed else an UPDATE will take place.
        /// </summary>
        /// <param name="recipeID">The ID of the recipe being rated</param>
        /// <param name="rating">A 1-5 rating</param>
        /// <param name="userEmail">The email of the current user</param>
        public void UpsertTasteRating(
            int id,
            int rating,
            int userID)
        {
            string sql = @"
                REPLACE INTO Taste_Rating
                    (userId, recipeId, rating, dateAdded)
                VALUES
                    (@userID, @recipeID, @rating, NOW())";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand insertCommand = new MySqlCommand(sql, connection))
                {
                    insertCommand.Parameters.AddWithValue("@userID", userID);
                    insertCommand.Parameters.AddWithValue("@recipeID", id);
                    insertCommand.Parameters.AddWithValue("@rating", rating);
                    insertCommand.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Used for REPLACING a Difficulty rating record. If the record is new
        /// and INSERT will be performed else an UPDATE will take place.
        /// </summary>
        /// <param name="recipeID">The ID of the recipe being rated</param>
        /// <param name="rating">A 1-5 rating</param>
        /// <param name="userEmail">The email of the current user</param>
        public void UpsertDifficultyRating(
            int id,
            int rating,
            int userID)
        {
            string sql = @"
                REPLACE INTO Difficulty_Rating
                    (userId, recipeId, rating, dateAdded)
                VALUES
                    (@userID, @recipeID, @rating, NOW())";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand insertCommand = new MySqlCommand(sql, connection))
                {
                    insertCommand.Parameters.AddWithValue("@userID", userID);
                    insertCommand.Parameters.AddWithValue("@recipeID", id);
                    insertCommand.Parameters.AddWithValue("@rating", rating);
                    insertCommand.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Deletes a specific recipe from the database
        /// </summary>
        /// <param name="recipe">The recipe to be deleted</param>
        /// <param name="userID"></param>
        /// <param name="dateDeleted"></param>
        /// <returns>Result of transaction as a bool value</returns>
        public bool DeleteRecipe(Recipe recipe)
        {
            MySqlTransaction sqlTransaction;
            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                sqlTransaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);

                string deleteRecipeString = @"
                        DELETE FROM Recipe WHERE id = @id";
                string deleteRecipeIngredientString = @"
                        DELETE FROM Recipe_Ingredient WHERE recipeId = @recipeID";
                string deleteInstructionString = @"
                        DELETE FROM Instruction WHERE recipeId = @recipeID";

                using (MySqlCommand deleteRecipeCommand = new MySqlCommand(deleteRecipeString, connection, sqlTransaction),
                        deleteRecipeIngredientCommand = new MySqlCommand(deleteRecipeIngredientString, connection, sqlTransaction),
                        deleteInstructionCommand = new MySqlCommand(deleteInstructionString, connection, sqlTransaction))
                {
                    deleteInstructionCommand.Parameters.AddWithValue("@recipeID", recipe.Id);
                    int deleteInstructionResult = deleteInstructionCommand.ExecuteNonQuery();

                    if (deleteInstructionResult > 0)
                    {
                        deleteRecipeIngredientCommand.Parameters.AddWithValue("@recipeID", recipe.Id);
                        int deleteRecipeIngredientResult = deleteRecipeIngredientCommand.ExecuteNonQuery();

                        if (deleteRecipeIngredientResult > 0)
                        {
                            deleteRecipeCommand.Parameters.AddWithValue("@id", recipe.Id);
                            int deleteRecipeResult = deleteRecipeCommand.ExecuteNonQuery();

                            if (deleteRecipeResult <= 0)
                            {
                                sqlTransaction.Rollback();
                                return false;
                            }
                        }
                        else
                        {
                            sqlTransaction.Rollback();
                            return false;
                        }
                        sqlTransaction.Commit();
                        return true;
                    }
                    else
                    {
                        sqlTransaction.Rollback();
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the id number of the last entered recipe
        /// </summary>
        /// <returns>the id number of the last entered recipe</returns>
        public int GetLastRecipeID()
        {
            int recipeID = 0;
            string selectCommandString = @"SELECT id FROM Recipe ORDER BY id DESC LIMIT 1";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                MySqlCommand selectCommand = new MySqlCommand(selectCommandString, connection);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        recipeID = Int32.Parse(reader["id"].ToString());
                    }
                }
            }
            return recipeID;
        }

        private bool CheckForExistingIngredients(List<RecipeIngredient> recipeIngredients, int userID, DateTime dateAdded)
        {
            var succeeded = true;
            IngredientDAL ingredientDAL = new IngredientDAL();

            List<Ingredient> ingredients = new List<Ingredient>();
            foreach (RecipeIngredient r in recipeIngredients)
            {
                var currentIngredient = new Ingredient(r.Ingredient);
                ingredients.Add(currentIngredient);
            }

            MySqlTransaction sqlTransaction;

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                sqlTransaction = connection.BeginTransaction(System.Data.IsolationLevel.RepeatableRead);

                string searchCommandString = @"
                SELECT * FROM Ingredient WHERE name=@ingredientName";

                using (MySqlCommand searchCommand = new MySqlCommand(searchCommandString, connection, sqlTransaction))
                {
                    foreach (Ingredient i in ingredients)
                    {
                        searchCommand.Parameters.AddWithValue("@ingredientName", i.Name);
                        using (var reader = searchCommand.ExecuteReader())
                        {
                            if (!reader.Read())
                            {
                                succeeded = ingredientDAL.AddIngredient(i, userID, dateAdded);
                                if (!succeeded)
                                {
                                    sqlTransaction.Rollback();
                                    return succeeded;
                                }
                            }
                            searchCommand.Parameters.Clear();
                        }
                    }
                    sqlTransaction.Commit();
                    return succeeded;
                }
            }
        }

        /// <summary>
        /// Returns the total count of active recipes
        /// </summary>
        /// <returns>Count of active recipes</returns>
        public int GetTotalRecipeCount()
        {
            return this.GetTotalRecipeCount(-1);
        }

        /// <summary>
        /// Returns the total count of active recipes for a given User
        /// </summary>
        /// <param name="userID">The ID of the User</param>
        /// <returns>Count of active recipes</returns>
        public int GetTotalRecipeCount(int userID)
        {
            string sql = @"
                SELECT
                    COUNT(*) AS total
                FROM
                    Recipe
                WHERE
                    dateDeleted IS NULL";

            if (userID > -1)
            {
                sql += " AND userID = @userID";
            }

            int recipeCount = 0;
            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand command = new MySqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@userID", userID);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            recipeCount = Convert.ToInt32(reader["total"]);
                        }
                    }
                }   
            }

            return recipeCount;
        }

        /// <summary>
        /// Sets the dateDeleted field to Now for a given recipe
        /// </summary>
        /// <param name="id">The Id of the recipe being deleted</param>
        public bool DeleteRecipe(int id)
        {
            string deleteRecipeSQL = @"
                UPDATE
                    Recipe
                SET
                    dateDeleted = NOW()
                WHERE
                    id = @id";

            string deleteTasteRatingSQL = @"
                UPDATE
                    Taste_Rating
                SET
                    dateDeleted = NOW()
                WHERE
                    recipeId = @id";

            string deleteDifficultyRatingSQL = @"
                UPDATE
                    Difficulty_Rating
                SET
                    dateDeleted = NOW()
                WHERE
                    recipeId = @id";

            string deleteIngredientsMappingSQL = @"
                UPDATE
                    Recipe_Ingredient
                SET
                    dateDeleted = NOW()
                WHERE
                    recipeId = @id";

            string deleteInstructionSQL = @"
                UPDATE
                    Instruction
                SET
                    dateDeleted = NOW()
                WHERE
                    recipeId = @id";


            MySqlTransaction transaction;

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                transaction = connection.BeginTransaction(System.Data.IsolationLevel.RepeatableRead);

                using (
                    MySqlCommand
                        deleteTastingRatingCommand = new MySqlCommand(deleteTasteRatingSQL, connection, transaction),
                        deleteDifficultyRatingCommand = new MySqlCommand(deleteDifficultyRatingSQL, connection, transaction),
                        deleteIngredientsMappingCommand = new MySqlCommand(deleteIngredientsMappingSQL, connection, transaction),
                        deleteInstructionCommand = new MySqlCommand(deleteInstructionSQL, connection, transaction),
                        deleteRecipeCommand = new MySqlCommand(deleteRecipeSQL, connection, transaction))
                {
                    deleteTastingRatingCommand.Parameters.AddWithValue("@id", id);
                    deleteTastingRatingCommand.ExecuteNonQuery();

                    deleteDifficultyRatingCommand.Parameters.AddWithValue("@id", id);
                    deleteDifficultyRatingCommand.ExecuteNonQuery();

                    deleteIngredientsMappingCommand.Parameters.AddWithValue("@id", id);
                    deleteIngredientsMappingCommand.ExecuteNonQuery();

                    deleteInstructionCommand.Parameters.AddWithValue("@id", id);
                    deleteInstructionCommand.ExecuteNonQuery();

                    deleteRecipeCommand.Parameters.AddWithValue("@id", id);
                    int result = deleteRecipeCommand.ExecuteNonQuery();
                    
                    if (result <= 0)
                    {
                        transaction.Rollback();
                        return false;
                    }

                    transaction.Commit();
                    return true;
                }
            }
        }
    }
}

