﻿using MySqlConnector;

namespace TheKitchen.DAL
{

    /// <summary>
    /// Connects to the 'theKitchen' Database.
    /// </summary>
    public class Connection
    {

        /// <summary>
        /// Returns the database connection string.
        /// </summary>
        public static MySqlConnection GetConnection()
        {
#if DEBUG
            string connectionString =
                "server=127.0.0.1;user=azure;pwd=f3SvgbdMmUDWLd2w;database=thekitchen;";
#else
            string connectionString =
                "server=127.0.0.1;user=azure;pwd=f3SvgbdMmUDWLd2w;database=thekitchen;Port=55599;";
#endif


            return new MySqlConnection(connectionString);
        }
    }
}
