﻿using System;
using System.Collections.Generic;
using MySqlConnector;
using TheKitchen.Models;

namespace TheKitchen.DAL
{
    /// <summary>
    /// The IngredientDAL class
    /// </summary>
    public class IngredientDAL
    {
        /// <summary>
        /// Returns a list of all ingredients in the database
        /// </summary>
        /// <returns>a list of all ingredients in the database</returns>
        public List<Ingredient> GetAllIngredients()
        {
            List<Ingredient> ingredients = new List<Ingredient>();

            string selectCommandString = @"
                SELECT * FROM Ingredient";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                MySqlCommand selectCommand = new MySqlCommand(selectCommandString, connection);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        ingredients.Add(new Ingredient(
                            reader["name"].ToString()));
                    }
                }
            }
            return ingredients;
        }

        /// <summary>
        /// Returns the ID number of a specific ingredient
        /// </summary>
        /// <param name="recipeIngredient">The RecipeIngredient to get ingredient name from</param>
        /// <returns>the ID number of a specific ingredient</returns>
        public int GetIngredientID(RecipeIngredient recipeIngredient)
        {
            int ingredientID = 0;
            string selectCommandString = @"SELECT * FROM Ingredient WHERE name=@name";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                MySqlCommand selectCommand = new MySqlCommand(selectCommandString, connection);
                selectCommand.Parameters.AddWithValue("@name", recipeIngredient.Ingredient);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (!int.TryParse(reader["id"].ToString(), out ingredientID))
                        {
                            return 0;
                        }
                    }
                    return ingredientID;
                }
            }
        }

        /// <summary>
        /// Adds a new ingredient to the database
        /// </summary>
        /// <param name="ingredient">The ingredient to add</param>
        /// <param name="user">The user adding the ingredient</param>
        /// <param name="dateAdded">The date of creation</param>
        /// <returns>A boolean value indicating the success of the creation</returns>
        public bool AddIngredient(Ingredient ingredient, int userID, DateTime dateAdded)
        {
            string insertCommandString = @"INSERT INTO Ingredient (userId, name, dateAdded) VALUES (@userID, @name, @dateAdded)";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                MySqlCommand insertCommand = new MySqlCommand(insertCommandString, connection);
                insertCommand.Parameters.AddWithValue("@userID", userID);
                insertCommand.Parameters.AddWithValue("@name", ingredient.Name.ToUpper());
                insertCommand.Parameters.AddWithValue("@dateAdded", dateAdded);
                var insertSucceeded = insertCommand.ExecuteNonQuery();

                if (insertSucceeded < 1)
                {
                    return false;
                }
                return true;
            }
        }
    }
}
