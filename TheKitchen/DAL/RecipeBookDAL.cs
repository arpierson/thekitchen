using System;
using System.Collections.Generic;
using System.Data;
using MySqlConnector;
using TheKitchen.Models;

namespace TheKitchen.DAL
{
    /// <summary>
    /// RecipeDAL Class
    /// </summary>
    public class RecipeBookDAL
    {
        /// <summary>
        /// Retrieves all RecipeBooks for a specific user
        /// </summary>
        /// <param name="userID">The userID for the user that owns the Recipe Books</param>
        /// <returns>A list of RecipeBooks for a specific user</returns>
        public List<RecipeBook> GetRecipeBooksForUser(int userID)
        {
            List<RecipeBook> recipeBooks = new List<RecipeBook>();
            List<RecipeBookEntry> recipes = new List<RecipeBookEntry>();
            RecipeDAL recipeDAL = new RecipeDAL();

            string selectBooksStatement = @"
                SELECT *
                FROM Recipe_Book
                WHERE
                userId = @userID AND dateDeleted IS NULL";

            string selectBookEntriesStatement = @"
                SELECT e.id, e.recipeBookId, e.recipeId, e.dateAdded
                FROM Recipe_Book_Entry AS e
                INNER JOIN Recipe AS r
                ON e.recipeId = r.id
                WHERE
                e.recipeBookId = @recipeBookID AND r.dateDeleted IS NULL";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand bookCommand = new MySqlCommand(selectBooksStatement, connection),
                        entryCommand = new MySqlCommand(selectBookEntriesStatement, connection))
                {
                    bookCommand.Parameters.AddWithValue("@userID", userID);

                    using (MySqlDataReader reader = bookCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            recipeBooks.Add(new RecipeBook
                            {
                                Id = Convert.ToInt32(reader["id"]),
                                Name = reader["name"].ToString(),
                                UserID = Convert.ToInt32(reader["userId"]),
                                DateAdded = (DateTime)reader["dateAdded"]
                            });
                        }
                    }

                    for (int i = 0; i < recipeBooks.Count; i++)
                    {
                        entryCommand.Parameters.Clear();
                        entryCommand.Parameters.AddWithValue("@recipeBookID", recipeBooks[i].Id);
                        using (MySqlDataReader reader = entryCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                recipes.Add(new RecipeBookEntry
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    RecipeBookId = Convert.ToInt32(reader["recipeBookId"]),
                                    Recipe = recipeDAL.GetRecipe(Convert.ToInt32(reader["recipeId"])),
                                    DateAdded = (DateTime)reader["dateAdded"]
                                });
                            }
                        }
                        recipeBooks[i].Recipes = recipes;
                    }
                }
            }
            return recipeBooks;
        }

        /// <summary>
        /// Returns a RecipeBook
        /// </summary>
        /// <param name="id">ID of the recipe book to be returned</param>
        /// <returns>A recipe book</returns>
        public RecipeBook GetRecipeBook(int id)
        {
            RecipeBook book = new RecipeBook();
            book.Recipes = new List<RecipeBookEntry>();
            RecipeDAL recipeDAL = new RecipeDAL();

            string selectBooksStatement = @"
                SELECT *
                FROM Recipe_Book
                WHERE
                id = @ID AND dateDeleted IS NULL";

            string selectBookEntriesStatement = @"
                SELECT e.id, e.recipeBookId, e.recipeId, e.dateAdded
                FROM Recipe_Book_Entry AS e
                INNER JOIN Recipe AS r
                ON e.recipeId = r.id
                WHERE
                e.recipeBookId = @recipeBookID AND e.dateDeleted IS NULL AND r.dateDeleted IS NULL";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand bookCommand = new MySqlCommand(selectBooksStatement, connection),
                        entryCommand = new MySqlCommand(selectBookEntriesStatement, connection))
                {
                    bookCommand.Parameters.AddWithValue("@ID", id);

                    using (MySqlDataReader reader = bookCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            book.Id = Convert.ToInt32(reader["id"]);
                            book.Name = reader["name"].ToString();
                            book.UserID = Convert.ToInt32(reader["userId"]);
                            book.DateAdded = (DateTime)reader["dateAdded"];
                        }
                    }

                    entryCommand.Parameters.AddWithValue("@recipeBookID", book.Id);
                    using (MySqlDataReader reader = entryCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            book.Recipes.Add(new RecipeBookEntry
                            {
                                Id = Convert.ToInt32(reader["id"]),
                                RecipeBookId = Convert.ToInt32(reader["recipeBookId"]),
                                Recipe = recipeDAL.GetRecipe(Convert.ToInt32(reader["recipeId"])),
                                DateAdded = (DateTime)reader["dateAdded"]
                            });
                        }
                    }
                }
            }
            return book;
        }

        /// <summary>
        /// Creates a new RecipeBook in the Recipe_Book database table
        /// </summary>
        /// <param name="userID">The user creating the book</param>
        /// <param name="name">The name of the book</param>
        /// <returns>Integer of rows affected by SQL command</returns>
        public int CreateRecipeBook(int userID, string name)
        {
            string createBookStatement = @"
                INSERT INTO Recipe_Book (userId, name, dateAdded)
                VALUES (@userID, @name, NOW())";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand createBookCommand = new MySqlCommand(createBookStatement, connection))
                {
                    createBookCommand.Parameters.AddWithValue("@userID", userID);
                    createBookCommand.Parameters.AddWithValue("@name", name);

                    return createBookCommand.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Adds a recipe to a recipe book
        /// </summary>
        /// <param name="bookID">The recipe book to add a recipe to</param>
        /// <param name="recipe">The recipe to be added to the book</param>
        /// <returns>Integer of rows affected by SQL command</returns>
        public int AddRecipeBookEntryToRecipeBook(int bookID, Recipe recipe)
        {
            string addRecipeStatement = @"
                INSERT INTO Recipe_Book_Entry (recipeBookId, recipeId, dateAdded)
                VALUES (@recipeBookID, @recipeID, NOW())";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand addRecipeCommand = new MySqlCommand(addRecipeStatement, connection))
                {
                    addRecipeCommand.Parameters.AddWithValue("@recipeBookID", bookID);
                    addRecipeCommand.Parameters.AddWithValue("@recipeID", recipe.Id);

                    return addRecipeCommand.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Soft deletes a recipe book and its entries from the DB
        /// </summary>
        /// <param name="bookID">The ID of the recipe book to be deleted</param>
        /// <returns>Integer of number of Recipe_Book table rows affected</returns>
        public int DeleteRecipeBook(int bookID)
        {
            string deleteRecipeBookSQL = @"
                UPDATE Recipe_Book 
                SET dateDeleted = NOW() 
                WHERE id = @ID";

            string deleteRecipeBookEntrySQL = @"
                UPDATE Recipe_Book_Entry 
                SET dateDeleted = NOW() 
                WHERE recipeBookId = @bookID";

            MySqlTransaction sqlTransaction;
            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                sqlTransaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                using (MySqlCommand deleteRecipeBookCommand = new MySqlCommand(deleteRecipeBookSQL, connection, sqlTransaction),
                        deleteRecipeBookEntryCommand = new MySqlCommand(deleteRecipeBookEntrySQL, connection, sqlTransaction))
                {
                    deleteRecipeBookEntryCommand.Parameters.AddWithValue("@bookID", bookID);
                    int deleteEntryResult = deleteRecipeBookEntryCommand.ExecuteNonQuery();

                    deleteRecipeBookCommand.Parameters.AddWithValue("@ID", bookID);
                    int deleteBookResult = deleteRecipeBookCommand.ExecuteNonQuery();

                    if (deleteBookResult == 0)
                    {
                        sqlTransaction.Rollback();
                        return deleteBookResult;
                    }

                    sqlTransaction.Commit();
                    return deleteBookResult;
                }
            }
        }

        public int DeleteRecipeFromRecipeBook(int bookEntryID)
        {
            string deleteRecipeSQL = @"
                UPDATE Recipe_Book_Entry 
                SET dateDeleted = NOW() 
                WHERE id = @ID";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand deleteRecipeFromBookCommand = new MySqlCommand(deleteRecipeSQL, connection))
                {
                    deleteRecipeFromBookCommand.Parameters.AddWithValue("@ID", bookEntryID);
                    return deleteRecipeFromBookCommand.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Checks for the presence of a recipe in a given recipe book
        /// </summary>
        /// <param name="bookID">The RecipeBook to check</param>
        /// <param name="recipe">The recipe being checked against</param>
        /// <returns>True or false if recipe already exists in list</returns>
        public bool CheckForDuplicateRecipesInBook(int bookID, Recipe recipe)
        {
            RecipeBook book = this.GetRecipeBook(bookID);
            bool alreadyExists = false;

            foreach (RecipeBookEntry r in book.Recipes)
            {
                if (r.Recipe.Id == recipe.Id)
                {
                    alreadyExists = true;
                    return alreadyExists;
                }
            }
            return alreadyExists;
        }

        /// <summary>
        /// Retrieves the ID of the newest DB entry for Recipe_Book
        /// </summary>
        /// <returns>The ID of the newest Recipe Book in the DB</returns>
        public int GetLastRecipeBookID()
        {
            int bookID = 0;
            string selectCommandString = @"SELECT id FROM Recipe_Book ORDER BY id DESC LIMIT 1";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                MySqlCommand selectCommand = new MySqlCommand(selectCommandString, connection);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        bookID = Int32.Parse(reader["id"].ToString());
                    }
                }
            }
            return bookID;
        }

        /// <summary>
        /// Retrieves the ID of the newest DB entry for Recipe_Book_Entry
        /// </summary>
        /// <returns>The ID of the newest Recipe Book Entry in the DB</returns>
        public int GetLastRecipeBookEntryID()
        {
            int entryID = 0;
            string selectCommandString = @"SELECT id FROM Recipe_Book_Entry ORDER BY id DESC LIMIT 1";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                MySqlCommand selectCommand = new MySqlCommand(selectCommandString, connection);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        entryID = Int32.Parse(reader["id"].ToString());
                    }
                }
            }
            return entryID;
        }

        /// <summary>
        /// Retrieves the ID of the newest DB entry for Recipe_Book_Entry
        /// </summary>
        /// <returns>The ID of the newest Recipe Book Entry in the DB</returns>
        public int GetLastRecipeEntryID()
        {
            int entryID = 0;
            string selectCommandString = @"SELECT id FROM Recipe_Book_Entry ORDER BY id DESC LIMIT 1";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                MySqlCommand selectCommand = new MySqlCommand(selectCommandString, connection);
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        entryID = Int32.Parse(reader["id"].ToString());
                    }
                }
            }
            return entryID;
        }

        /// <summary>
        /// Removes a recipe book from the database
        /// </summary>
        /// <param name="id">The id number of the book to be deleted</param>
        /// <returns>The number of rows affected in the DB</returns>
        public int DeleteRecipeBookInTests(int id)
        {
            string deleteBookStatment = @"
                DELETE FROM Recipe_Book
                WHERE id = @ID";

            string deleteBookEntryStatment = @"
                DELETE FROM Recipe_Book_Entry
                WHERE recipeBookId = @bookID";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand deleteCommand = new MySqlCommand(deleteBookStatment, connection),
                        deleteEntryCommand = new MySqlCommand(deleteBookEntryStatment, connection))
                {

                    deleteEntryCommand.Parameters.AddWithValue("@bookID", id);
                    deleteEntryCommand.ExecuteNonQuery();
                    deleteCommand.Parameters.AddWithValue("@ID", id);
                    return deleteCommand.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Removes a recipe book entry from the database
        /// </summary>
        /// <param name="recipeID">The id number of the recipe to be removed</param>
        /// <param name="bookID">The id number of the book from which the recipe is to be removed</param>
        /// <returns>The number of rows affected in the DB</returns>
        public int DeleteRecipeFromRecipeBookInTests(int id)
        {
            string deleteBookStatment = @"
                DELETE FROM Recipe_Book_Entry
                WHERE id = @ID";

            using (MySqlConnection connection = Connection.GetConnection())
            {
                connection.Open();
                using (MySqlCommand deleteCommand = new MySqlCommand(deleteBookStatment, connection))
                {
                    deleteCommand.Parameters.AddWithValue("@ID", id);

                    return deleteCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
