using System;
using System.Data;
using MySqlConnector;
using TheKitchen.Models;

namespace TheKitchen.DAL
{

    /// <summary>
    /// Queries the User table in the 'thekitchen' Database.
    /// </summary>
    public class UserDAL
    {
        /// <summary>
        /// Adds a new User to the User table in the 'thekitchen' Database.
        /// </summary>
        public UserModel AddUser(UserModel user)
        {
            string insertStatement =
               "INSERT INTO User(" +
                    "email, " +
                    "password, " +
                    "firstname, " +
                    "lastname, " +
                    "countryCode, " +
                    "picture, " +
                    "dateAdded) " +
               "VALUES(" +
                    "@email, " +
                    "@password, " +
                    "@firstname, " +
                    "@lastname, " +
                    "@countryCode, " +
                    "@picture, " +
                    "Now());" +
                "SELECT " +
                    "id FROM User WHERE email=@email";

            using MySqlConnection connection = Connection.GetConnection();
            using MySqlCommand command = new MySqlCommand(insertStatement, connection);

            command.Parameters.AddWithValue("@email", user.Email);
            command.Parameters.AddWithValue("@password", Utilities.Hashing.GetHash(user.Password));
            command.Parameters.AddWithValue("@firstname", user.FirstName);
            command.Parameters.AddWithValue("@lastname", user.LastName);
            command.Parameters.AddWithValue("@countryCode", user.CountryCode);
            command.Parameters.AddWithValue("@picture", user.Image);

            connection.Open();
            int userID = (int)command.ExecuteScalar();
            user.UserId = userID;

            return user;
        }

        /// <summary>
        /// Authenticates a User trying to log in.
        /// </summary>
        public UserModel AuthenticateUser(string email, string password)
        {
            UserModel user = null;

            string selectStatement = @"
                SELECT
                    id,
                    firstname,
                    lastname,
                    email,
                    countryCode,
                    password,
                    picture,
                    dateAdded
                FROM
                    User
                WHERE
                    email = @email";

            using MySqlConnection connection = Connection.GetConnection();
            MySqlCommand command = new MySqlCommand(selectStatement, connection);

            command.Parameters.AddWithValue("@email", email);
            connection.Open();

            using MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (!TheKitchen.Utilities.Hashing.VerifyHash(password, reader["password"].ToString()))
                {
                    break;
                }

                byte[] picture = null;
                if (reader["picture"] != null & reader["picture"] != DBNull.Value)
                {
                    picture = (byte[])reader["picture"];
                }

                user = new UserModel
                {
                    UserId = Convert.ToInt32(reader["id"]),
                    FirstName = reader["firstname"].ToString(),
                    LastName = reader["lastname"].ToString(),
                    Email = reader["email"].ToString(),
                    CountryCode = reader["countryCode"].ToString(),
                    Image = picture,
                    DateAdded = (DateTime)reader["dateAdded"]
                };
            }
            return user;
        }

        /// <summary>
        /// Returns a User from the User table in the 'thekitchen' Database.
        /// </summary>
        public UserModel GetUser(string email)
        {
            UserModel user = null;

            string selectStatement =

                "SELECT " +
                    "id, " +
                    "firstname, " +
                    "lastname, " +
                    "email, " +
                    "countryCode, " +
                    "password, " +
                    "picture, " +
                    "dateAdded " +
                "FROM " +
                    "User " +
                "WHERE " +
                    "email = @email";

            using MySqlConnection connection = Connection.GetConnection();
            using MySqlCommand command = new MySqlCommand(selectStatement, connection);

            command.Parameters.AddWithValue("@email", email);

            connection.Open();
            using MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                byte[] picture = null;
                if (reader["picture"] != null & reader["picture"] != DBNull.Value)
                {
                    picture = (byte[])reader["picture"];
                }

                user = new UserModel
                {
                    UserId = Convert.ToInt32(reader["id"]),
                    FirstName = reader["firstname"].ToString(),
                    LastName = reader["lastname"].ToString(),
                    Email = reader["email"].ToString(),
                    CountryCode = reader["countryCode"].ToString(),
                    Image = picture,
                    DateAdded = (DateTime)reader["dateAdded"],
                    Password = reader["password"].ToString(),
                };
            }
            return user;
        }

        /// <summary>
        /// Returns a User from the User table in the 'thekitchen' Database.
        /// </summary>
        public bool UpdateUser(UserModel oldUser, UserModel newUser)
        {
            string updateStatement =
                    "UPDATE User SET " +
                        "firstname = @newFirstName, " +
                        "lastname = @newLastName, " +
                        "email = @newEmail, " +
                        "countryCode = @newCountryCode, ";

            if (!String.IsNullOrEmpty(newUser.Password))
            {
                updateStatement += "password = @newPassword, ";
            }

            updateStatement +=
                        "picture = @newPicture, " +
                        "dateModified = Now() " +
                    "WHERE " +
                        "email = @oldEmail";

            using MySqlConnection connection = Connection.GetConnection();
            using MySqlCommand command = new MySqlCommand(updateStatement, connection);

            command.Parameters.AddWithValue("@oldId", oldUser.UserId);
            command.Parameters.AddWithValue("@oldFirstName", oldUser.FirstName);
            command.Parameters.AddWithValue("@oldLastName", oldUser.LastName);
            command.Parameters.AddWithValue("@oldEmail", oldUser.Email);
            command.Parameters.AddWithValue("@oldCountryCode", oldUser.CountryCode);
            command.Parameters.AddWithValue("@oldDateAdded", oldUser.DateAdded);
            command.Parameters.AddWithValue("@newFirstName", newUser.FirstName);
            command.Parameters.AddWithValue("@newLastName", newUser.LastName);
            command.Parameters.AddWithValue("@newEmail", newUser.Email);
            command.Parameters.AddWithValue("@newCountryCode", newUser.CountryCode);

            if (!String.IsNullOrEmpty(newUser.Password))
            {
                command.Parameters.AddWithValue("@newPassword", Utilities.Hashing.GetHash(newUser.Password));
            }

            command.Parameters.AddWithValue("@newPicture", newUser.Image);
            command.Parameters.AddWithValue("@newDateAdded", newUser.DateAdded);

            connection.Open();
            int count = command.ExecuteNonQuery();

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns a User from the User table in the 'thekitchen' Database.
        /// </summary>
        public bool DeleteUser(string email)
        {

            MySqlTransaction sqlTransaction;
            using MySqlConnection connection = Connection.GetConnection();
            connection.Open();
            sqlTransaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);

            string deleteUserString = @"DELETE FROM User WHERE email = @email";

            using MySqlCommand deleteUserCommand = new MySqlCommand(deleteUserString, connection, sqlTransaction);
            deleteUserCommand.Parameters.AddWithValue("@email", email);
            int deleteRecipeResult = deleteUserCommand.ExecuteNonQuery();

            if (deleteRecipeResult > 0)
            {
                sqlTransaction.Commit();
                return true;
            }
            else
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public int GetUserRecipeTasteRating(string email, int recipeID)
        {
            string selectStatement = @"
                SELECT
                    rating
                FROM
                    Taste_Rating
                WHERE
                    userId IN (
                        SELECT
                            id
                        FROM
                            user
                        WHERE
                            email = @email)
                    AND dateDeleted IS NULL
                    AND recipeId = @recipeID
                ORDER BY
                    dateAdded DESC
                LIMIT 1";

            using MySqlConnection connection = Connection.GetConnection();
            using MySqlCommand command = new MySqlCommand(selectStatement, connection);

            command.Parameters.AddWithValue("@email", email);
            command.Parameters.AddWithValue("@recipeID", recipeID);

            connection.Open();
            using MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                return Convert.ToInt32(reader["rating"]);
            }
            return 0;
        }

        public int GetUserRecipeDifficultyRating(string email, int recipeID)
        {
            string selectStatement = @"
                SELECT
                    rating
                FROM
                    Difficulty_Rating
                WHERE
                    userId IN (
                        SELECT
                            id
                        FROM
                            user
                        WHERE
                            email = @email)
                    AND dateDeleted IS NULL
                    AND recipeId = @recipeID
                ORDER BY
                    dateAdded DESC
                LIMIT 1";

            using MySqlConnection connection = Connection.GetConnection();
            using MySqlCommand command = new MySqlCommand(selectStatement, connection);

            command.Parameters.AddWithValue("@email", email);
            command.Parameters.AddWithValue("@recipeID", recipeID);

            connection.Open();
            using MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                return Convert.ToInt32(reader["rating"]);
            }
            return 0;
        }
    }
}
    
    