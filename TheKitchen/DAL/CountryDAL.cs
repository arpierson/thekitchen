using System;
using System.Data;
using MySqlConnector;

namespace TheKitchen.DAL
{
    
    /// <summary>
    /// Queries the Country table in the 'thekitchen' Database.
    /// </summary>
    public class CountryDAL
    {

        /// <summary>
        /// Returns the codes and names of all Countries in the Country table in the 'thekitchen' Database.
        /// </summary>
        public DataTable GetCountryDataTable()
        {
            string selectStatement = "SELECT code, name " +
                                     "FROM Country " +
                                     "GROUP BY code, name " +
                                     "ORDER BY name";

            DataTable dataTable = new DataTable();

            using MySqlConnection connection = Connection.GetConnection();
            using MySqlCommand command = new MySqlCommand(selectStatement, connection);
            connection.Open();

            using MySqlDataReader reader = command.ExecuteReader();
            dataTable.Load(reader);
            return dataTable;
        }

        /// <summary>
        /// When given a country code, the full country name is returned
        /// </summary>
        /// <param name="code">Country Code</param>
        /// <returns>Full Country Name</returns>
        public String GetCountryFromCode(string code)
        {
            string country = "N/A";

            string sql = @"
                SELECT
                    name
                FROM
                    country
                WHERE
                    code = @code";

            using MySqlConnection connection = Connection.GetConnection();
            using MySqlCommand command = new MySqlCommand(sql, connection);
            connection.Open();

            command.Parameters.AddWithValue("@code", code);

            using MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                country = reader["name"].ToString();
            }
            return country;
        }
    }
}
