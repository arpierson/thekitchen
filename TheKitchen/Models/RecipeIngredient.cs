﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TheKitchen.Models
{
    /// <summary>
    /// Model class for a RecipeIngredient
    /// </summary>
    public class RecipeIngredient
    {
        public int Id { get; set; }

        public int IngredientID { get; set; }

        [Required]
        public string Measurement { get; set; }

        [Required]
        public string Ingredient { get; set; }

        public DateTime DateAdded { get; set; }
    }
}