﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TheKitchen.Models
{

    /// <summary>
    /// Model class for User.
    /// </summary>
    public class UserModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "First Name is required.")]
        [StringLength(45,
            ErrorMessage = "The First Name value cannot exceed 45 characters. ")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required.")]
        [StringLength(45,
            ErrorMessage = "The Last Name value cannot exceed 45 characters. ")]
        public string LastName{ get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Country is required.")]
        [StringLength(2, MinimumLength = 2,
            ErrorMessage = "Invalid country")]
        public string CountryCode { get; set; }

        [StringLength(45, ErrorMessage = "Password cannot exceed 45 characters. ")]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,45}$",
            ErrorMessage = "Passwords must be 8 to 45 characters and include a number,"+
                           "lowercase letter, uppercase letter, and special character")]
        public string Password { get; set; }

        public byte[] Image { get; set; }

        public DateTime DateAdded { get; set; }

        public DateTime DateModified { get; set; }

        public DateTime Datedeleted { get; set; }
    }
}
