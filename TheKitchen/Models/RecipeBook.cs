﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheKitchen.Models
{
    public class RecipeBook
    {
        public int Id { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "Name", Prompt = "Name")]
        public string Name { get; set; }

        [Required]
        public int UserID { get; set; }

        public List<RecipeBookEntry> Recipes { get; set; }

        public DateTime DateAdded { get; set; }
    }
}
