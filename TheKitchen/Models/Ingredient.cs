﻿using System.ComponentModel.DataAnnotations;

namespace TheKitchen.Models
{
    /// <summary>
    /// The model class for a recipe ingredient
    /// </summary>
    public class Ingredient
    {
        public int Id { get; set; }

        [Required]
        [StringLength(45, MinimumLength = 2)]
        public string Name { get; set; }

        public Ingredient(string name)
        {
            this.Name = name;
        }
    }
}
