﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheKitchen.Models
{
    public class AccountModel
    {
        [Required]
        public UserModel User { get; set; }

        [Required]
        public List<Recipe> Recipes { get; set; }

        [Required]
        public List<RecipeBook> RecipeBooks { get; set; }
    }
}
