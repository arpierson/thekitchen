﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TheKitchen.Models
{
    /// <summary>
    /// The model class for a recipe instruction
    /// </summary>
    public class Instruction
    {
        [Required]
        public int Step { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 2)]
        public string InstructionText { get; set; }

        public int Id { get; set; }

        public DateTime DateAdded { get; set; }
    }
}
