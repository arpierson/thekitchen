using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheKitchen.Models
{
    /// <summary>
    /// The model class for a recipe
    /// </summary>
    public class Recipe
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "User is required")]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [StringLength(45, MinimumLength = 2)]
        [Display(Name = "Title", Prompt = "Title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [StringLength(500)]
        [Display(Name = "Description", Prompt = "Description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "You must include at least one ingredient.")]
        public List<RecipeIngredient> Ingredients {get; set;}

        [Required(ErrorMessage = "You must include at least one instruction step.")]
        public List<Instruction> Directions { get; set; }

        public byte[] Picture { get; set; }

        public DateTime DateAdded { get; set; }
        
        public int TasteRating { get; set; }
        
        public int DifficultyRating { get; set; }

    }
}
