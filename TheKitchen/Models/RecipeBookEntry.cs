﻿using System;
namespace TheKitchen.Models
{
    /// <summary>
    /// Model class for a recipe book entry
    /// </summary>
    public class RecipeBookEntry
    {
        public int Id { get; set; }

        public int RecipeBookId { get; set; }

        public Recipe Recipe { get; set; }

        public DateTime DateAdded { get; set; }
    }
}
