﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.ControllerTests.AccountControllerTests
{
    [TestFixture]
    public class AccountController_RecipeBookDetailsTest
    {
        [Test]
        public void ShouldReturnPartialViewOfRecipeBookDetails()
        {
            RecipeBookDAL recipeBookDAL = new RecipeBookDAL();

            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new AccountController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            RecipeBook testBook = new RecipeBook
            {
                Name = "Unit test book",
                UserID = 1
            };

            tempData["RecipeBook"] = JsonConvert.SerializeObject(testBook); ;
            controller.TempData = tempData;

            PartialViewResult result = controller._RecipeBookDetails() as PartialViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("_RecipeBookDetails", result.ViewName);
        }
    }
}
