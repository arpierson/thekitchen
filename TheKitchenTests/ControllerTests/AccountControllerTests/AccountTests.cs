﻿using NUnit.Framework;
using Microsoft.AspNetCore.Mvc;
using TheKitchen.Controllers;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace TheKitchenTests.ControllerTests.AccountControllerTests
{
    [TestFixture]
    public class AccountTests
    {

        [Test]
        public void ShouldReturnNameAccountTest()
        {

            AccountController accountController = new AccountController();
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));

            accountController.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            ViewResult result = accountController.Account() as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("Account"));
        }

        [Test]
        public void ShouldReturnNameWithViewBagActiveTab()
        {
            AccountController accountController = new AccountController();
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));

            accountController.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            ViewResult result = accountController.Account("Test") as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("Account"));
        }
    }
}
