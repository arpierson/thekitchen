﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.ControllerTests.RecipeBookControllerTests
{
    [TestFixture]
    public class RecipeBookControllerPostCreateTests
    {
        [Test]
        public void ShouldCreateNewRecipeBook()
        {
            RecipeBookDAL recipeBookDAL = new RecipeBookDAL();

            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            RecipeBook testBook = new RecipeBook {
                Name = "Unit test book",
                UserID = 1
            };

            int oldRecipeBookCount = recipeBookDAL.GetRecipeBooksForUser(testBook.UserID).Count;
            RedirectToActionResult result = controller.Create(testBook) as RedirectToActionResult;
            int newRecipeBookCount = recipeBookDAL.GetRecipeBooksForUser(testBook.UserID).Count;

            Assert.AreNotEqual(oldRecipeBookCount, newRecipeBookCount);
            Assert.AreEqual(oldRecipeBookCount + 1, newRecipeBookCount);
            Assert.IsNotNull(result);
            Assert.AreEqual("Account", result.ActionName);

            Assert.AreEqual(recipeBookDAL.DeleteRecipeBookInTests(recipeBookDAL.GetLastRecipeBookID()), 1);
        }

        [Test]
        public void ShouldRedirectBackToAccountIfModelIsInvalid()
        {
            RecipeBookDAL recipeBookDAL = new RecipeBookDAL();

            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            RecipeBook testBook = new RecipeBook();

            controller.ModelState.AddModelError("Name", @"Name is required");

            RedirectResult result = controller.Create(testBook) as RedirectResult;

            Assert.AreEqual(result.Url, "/Account#recipe-books");
        }
    }
}
