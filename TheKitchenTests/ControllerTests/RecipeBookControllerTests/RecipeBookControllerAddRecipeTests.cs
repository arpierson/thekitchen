﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;

namespace TheKitchenTests.ControllerTests.RecipeBookControllerTests
{
    [TestFixture]
    public class RecipeBookControllerAddRecipeTests
    {
        [Test]
        public void ShouldAddRecipeToRecipeBook()
        {
            RecipeBookDAL recipeBookDAL = new RecipeBookDAL();

            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            int oldRecipeCount = recipeBookDAL.GetRecipeBook(2).Recipes.Count;
            JsonResult result = controller.AddRecipeBookEntryToRecipeBook(2, 1) as JsonResult;
            int newRecipeCount = recipeBookDAL.GetRecipeBook(2).Recipes.Count;

            Assert.AreNotEqual(oldRecipeCount, newRecipeCount);
            Assert.AreEqual(oldRecipeCount + 1, newRecipeCount);
            Assert.IsNotNull(result);

            Assert.AreEqual(recipeBookDAL.DeleteRecipeFromRecipeBookInTests(recipeBookDAL.GetLastRecipeEntryID()), 1);
        }

        [Test]
        public void ShouldReturn500StatusCodeIfBookIdIsZero()
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            ObjectResult result = controller.AddRecipeBookEntryToRecipeBook(0, 0) as ObjectResult;

            Assert.AreEqual(result.StatusCode, 500);
        }

        [Test]
        public void ShouldNotAddRecipeIfRecipeAlreadyExistsInBook()
        {
            RecipeBookDAL recipeBookDAL = new RecipeBookDAL();

            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            controller.AddRecipeBookEntryToRecipeBook(2, 1);
            int oldRecipeCount = recipeBookDAL.GetRecipeBook(2).Recipes.Count;
            JsonResult result = controller.AddRecipeBookEntryToRecipeBook(2, 1) as JsonResult;
            int newRecipeCount = recipeBookDAL.GetRecipeBook(2).Recipes.Count;

            Assert.AreEqual(oldRecipeCount, newRecipeCount);
            Assert.IsNotNull(result);

            Assert.AreEqual(recipeBookDAL.DeleteRecipeFromRecipeBookInTests(recipeBookDAL.GetLastRecipeEntryID()), 1);
        }
    }
}
