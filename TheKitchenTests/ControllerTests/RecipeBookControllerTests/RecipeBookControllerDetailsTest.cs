﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using TheKitchen.Controllers;

namespace TheKitchenTests.ControllerTests.RecipeBookControllerTests
{
    [TestFixture]
    public class RecipeBookControllerDetailsTest
    {
        [Test]
        public void ShouldReturnRedirectToActionResult()
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            RedirectToActionResult result = controller.Details(1) as RedirectToActionResult;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.ActionName, "_RecipeBookDetails");
        }
    }
}
