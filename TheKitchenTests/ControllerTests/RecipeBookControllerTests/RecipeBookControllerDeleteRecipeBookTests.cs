﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.ControllerTests.RecipeBookControllerTests
{
    [TestFixture]
    public class RecipeBookControllerDeleteRecipeBookTests
    {
        [Test]
        public void ShouldDeleteBook()
        {
            RecipeBookDAL recipeBookDAL = new RecipeBookDAL();
            RecipeDAL recipeDAL = new RecipeDAL();

            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            RecipeBook testBook = new RecipeBook
            {
                Name = "Unit test book",
                UserID = 1
            };

            controller.Create(testBook);
            var bookID = recipeBookDAL.GetLastRecipeBookID();

            int oldRecipeBookCount = recipeBookDAL.GetRecipeBooksForUser(testBook.UserID).Count;

            controller.AddRecipeBookEntryToRecipeBook(bookID, 1);

            JsonResult result = controller.DeleteRecipeBook(bookID) as JsonResult;
            int newRecipeBookCount = recipeBookDAL.GetRecipeBooksForUser(testBook.UserID).Count;

            Assert.AreNotEqual(oldRecipeBookCount, newRecipeBookCount);
            Assert.AreEqual(oldRecipeBookCount - 1, newRecipeBookCount);
            Assert.IsNotNull(result);
            Assert.AreEqual(recipeBookDAL.DeleteRecipeBookInTests(bookID), 1);
        }


        [Test]
        public void ShouldReturn500StatusCodeIfBookIdIsZero()
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            ObjectResult result = controller.DeleteRecipeBook(0) as ObjectResult;

            Assert.AreEqual(result.StatusCode, 500);
        }
    }
}