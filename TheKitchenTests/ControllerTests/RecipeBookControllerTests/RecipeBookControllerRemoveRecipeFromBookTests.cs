﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;

namespace TheKitchenTests.ControllerTests.RecipeBookControllerTests
{
    [TestFixture]
    public class RecipeBookControllerRemoveRecipeFromBookTests
    {
        [Test]
        public void ShouldRemoveRecipeBookEntryFromRecipeBook()
        {
            RecipeBookDAL recipeBookDAL = new RecipeBookDAL();

            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            controller.AddRecipeBookEntryToRecipeBook(2, 1);

            int oldRecipeCount = recipeBookDAL.GetRecipeBook(2).Recipes.Count;
            int entryID = recipeBookDAL.GetLastRecipeBookEntryID();
            JsonResult result = controller.DeleteRecipeBookEntryFromBook(entryID) as JsonResult;
            int newRecipeCount = recipeBookDAL.GetRecipeBook(2).Recipes.Count;

            Assert.AreEqual(newRecipeCount, oldRecipeCount - 1);
            Assert.IsNotNull(result);
            Assert.AreEqual(recipeBookDAL.DeleteRecipeFromRecipeBookInTests(entryID), 1);
        }

        [Test]
        public void ShouldReturn500StatusCodeIfBookIdIsZero()
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeBookController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;

            ObjectResult result = controller.DeleteRecipeBookEntryFromBook(0) as ObjectResult;

            Assert.AreEqual(result.StatusCode, 500);
        }
    }
}
