﻿using NUnit.Framework;
using TheKitchen.Controllers;

namespace TheKitchenTests.ControllerTests.UserControllerTests
{
    [TestFixture]
    public class GetUserControllerTests
    {
        [Test]
        public void ShouldCreateNewUserControllerTest()
        {
            UserController userController = new UserController();
            Assert.That(userController, Is.Not.Null);
        }
    }
}
