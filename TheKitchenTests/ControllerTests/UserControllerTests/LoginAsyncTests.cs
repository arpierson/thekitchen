﻿using NUnit.Framework;
using TheKitchen.Controllers;
using System.Threading.Tasks;

namespace TheKitchenTests.ControllerTests.UserControllerTests
{
    [TestFixture]
    public class LoginAsyncTests
    {

        [Test]
        public async Task ShouldReturnObjectResultTest()
        {
            UserController userController = new UserController();
            var result = await userController.LoginAsync("jrobbin4@my.westga.edu", "Kitchen!23");
        }

        [Test]
        public async Task ShouldReturnUnauthorizedWithNullEmailTest()
        {
            UserController userController = new UserController();
            var result = await userController.LoginAsync(null, "Kitchen!23");
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult", result.ToString());
        }

        [Test]
        public async Task ShouldReturnUnauthorizedWithNullPasswordTest()
        {
            UserController userController = new UserController();
            var result = await userController.LoginAsync("jrobbin4@my.westga.edu", null);
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult", result.ToString());
        }

        [Test]
        public async Task ShouldReturnUnauthorizedWithEmptyEmailTest()
        {
            UserController userController = new UserController();
            var result = await userController.LoginAsync("", "Kitchen!23");
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult", result.ToString());
        }

        [Test]
        public async Task ShouldReturnUnauthorizedWithEmptyPasswordTest()
        {
            UserController userController = new UserController();
            var result = await userController.LoginAsync("jrobbin4@my.westga.edu", "");
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult", result.ToString());
        }

        [Test]
        public async Task ShouldReturnUnauthorizedWithWrongEmailAndPasswordTest()
        {
            UserController userController = new UserController();
            var result = await userController.LoginAsync("jron4@my.stga.edu", "itche3");
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult", result.ToString());
        }
    }
}
