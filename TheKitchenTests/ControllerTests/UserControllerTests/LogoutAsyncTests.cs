﻿using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using TheKitchen.Controllers;
using Moq;
using Microsoft.AspNetCore.Authentication;
using System;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TheKitchenTests.ControllerTests.UserControllerTests
{

    [TestFixture]
    public class LogoutAsyncTests
    {

        [Test]
        public async System.Threading.Tasks.Task ShouldMakeCallToLogoutMethodTestAsync()
        {
           
            var authServiceMock = new Mock<IAuthenticationService>();
            authServiceMock
                .Setup(_ => _.SignInAsync(It.IsAny<HttpContext>(), It.IsAny<string>(), It.IsAny<ClaimsPrincipal>(), It.IsAny<AuthenticationProperties>()))
                .Returns(Task.FromResult((object)null));

            var serviceProviderMock = new Mock<IServiceProvider>();
            serviceProviderMock
                .Setup(_ => _.GetService(typeof(IAuthenticationService)))
                .Returns(authServiceMock.Object);

            UserController userController = new UserController()
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        // How mock RequestServices?
                        RequestServices = serviceProviderMock.Object
                    }
                }
            };

            RedirectResult result = await userController.LogoutAsync();
            Assert.That(result.Url, Is.EqualTo("/Home"));
        }
    }
}
