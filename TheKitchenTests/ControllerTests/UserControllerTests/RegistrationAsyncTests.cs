﻿using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;

namespace TheKitchenTests.ControllerTests.UserControllerTests
{

    [TestFixture]
    public class RegistrationAsyncTests
    {

        [Test]
        public async System.Threading.Tasks.Task ShouldReturnForbidWhenPotDoesNotEqualHoneyTest()
        {
            UserController userController = new UserController();
            var result = await userController.RegistrationAsync("Eden", "Robbins", "eden@eden", "US", "Eden!234", "Eden!234", "No", null);
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.ForbidResult", result.ToString());
        }

        [Test]
        public async System.Threading.Tasks.Task ShouldReturnUnauthorizedWhenPasswordsDoNotMatchTest()
        {
            UserController userController = new UserController();
            var result = await userController.RegistrationAsync("Eden", "Robbins", "eden@eden", "US", "Eden!234", "Ed34", "honey", null);
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult", result.ToString());
        }

        [Test]
        public async System.Threading.Tasks.Task ShouldRegisterUserAndReturnObjectResultTest()
        {
            UserController userController = new UserController();
            var result = await userController.RegistrationAsync("Eden", "Robbins", "eden@eden", "US", "Eden!234", "Eden!234", "honey", null);
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.ObjectResult", result.ToString());

            UserDAL userDal = new UserDAL();
            userDal.DeleteUser("eden@eden");
        }

        [Test]
        public async System.Threading.Tasks.Task ShouldUpdateUserAndReturnObjectResultTest()
        {
            UserController userController = new UserController();
            var result1 = await userController.RegistrationAsync("Eden", "Robbins", "eden@eden", "US", "Eden!234", "Eden!234", "honey", null);
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.ObjectResult", result1.ToString());

            var result2 = await userController.RegistrationAsync("Mary", "Poppins", "mary@mary", "US", "Eden!234", "Eden!234", "honey", null);
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.ObjectResult", result2.ToString());

            UserDAL userDal = new UserDAL();
            userDal.DeleteUser("eden@eden");
            userDal.DeleteUser("mary@mary");
        }
    }
}
