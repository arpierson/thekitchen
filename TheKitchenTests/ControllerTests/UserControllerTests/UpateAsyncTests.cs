﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.ControllerTests.UserControllerTests
{
    public class UpdateAsyncTests
    {
        UserController userController;
        UserModel userToUpdate;
        UserModel updatedUser;
        UserDAL userDAL;

        [SetUp]
        public void Init()
        {
            ClaimsPrincipal claimsUser = new ClaimsPrincipal();
            this.userController = new UserController();
            this.userDAL = new UserDAL();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.Email, "bdepoin1@my.westga.edu")
            }, "mock"));

            this.userController.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            this.userToUpdate = this.userDAL.GetUser("bdepoin1@my.westga.edu");
        }

        [TearDown]
        public void ResetUser()
        {
            this.userDAL.UpdateUser(this.updatedUser, this.userToUpdate);
        }

        [Test]
        [TestCase(
            "Benny",
            "Boy",
            "bdp@bdp.com",
            "NL",
            null,
            "",
            "")]
        [TestCase(
            "Ahkmed",
            "De Point",
            "bdp@bdp123.com",
            "SA",
            null,
            "",
            "")]
        [TestCase(
            "",
            "",
            "bdp@bdp456.com",
            null,
            null,
            "Poop13P@nts",
            "Poop13P@nts")]
        public async System.Threading.Tasks.Task ShouldUpdateUser(
            string firstName,
            string lastName,
            string email,
            string countryCode,
            IFormFile image,
            string password,
            string confirmPassword
            )
        {
            if (String.IsNullOrEmpty(firstName))
            {
                firstName = this.userToUpdate.FirstName;
            }
            if (String.IsNullOrEmpty(lastName))
            {
                lastName = this.userToUpdate.LastName;
            }
            if (String.IsNullOrEmpty(email))
            {
                email = this.userToUpdate.Email;
            }
            if (String.IsNullOrEmpty(countryCode))
            {
                countryCode = this.userToUpdate.CountryCode;
            }

            var result = await this.userController.UpdateUserAsync(
                firstName,
                lastName,
                email,
                countryCode,
                password,
                confirmPassword,
                image
            );

            this.updatedUser = new UserModel();
            if (email != this.userToUpdate.Email)
            {
                this.updatedUser = userDAL.GetUser(email);
            }
            else
            {
                this.updatedUser = userDAL.GetUser(this.userToUpdate.Email);
            }
            
            Assert.AreEqual(firstName, this.updatedUser.FirstName);
            Assert.AreEqual(lastName, this.updatedUser.LastName);
            Assert.AreEqual(email, this.updatedUser.Email);
            Assert.AreEqual(countryCode, this.updatedUser.CountryCode);

            if (image != null)
            {
                Assert.AreEqual(TheKitchen.Utilities.Images.ConvertUploadedImage(image), this.updatedUser.Image);
            }
            if (!String.IsNullOrEmpty(password))
            {
                Assert.IsTrue(TheKitchen.Utilities.Hashing.VerifyHash(password
                    , this.updatedUser.Password));
            }
        }
    }
}
