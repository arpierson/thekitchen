﻿using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.ControllerTests.RecipeControllerTests
{
    [TestFixture]
    public class RecipeControllerPostCreateTests
    {
        RecipeController controller;
        ClaimsPrincipal claimsUser;

        [Test]
        public void RecipeCreateShouldRedirectToCreateViewIfModelIsInvalid()
        {
            controller = new RecipeController();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            Recipe recipe = new Recipe();
            controller.ModelState.AddModelError("Title", @"Title is required");

            ViewResult result = controller.Create(recipe, null) as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("Create"));
        }

        [Test]
        public void RecipeCreateShouldWriteNewRecipeToDatabaseIfModelIsValid()
        {
            controller = new RecipeController();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            RecipeDAL recipeData = new RecipeDAL();
            List<RecipeIngredient> _ingredients = new List<RecipeIngredient>();
            List<Instruction> _instructions = new List<Instruction>();

            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 cup",
                Ingredient = "Sugar"
            });

            _instructions.Add(new Instruction
            {
                Step = 1,
                InstructionText = "Make food"
            });

            Recipe recipe = new Recipe
            {
                Title = "Test",
                Description = "Test description",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            int oldRecipeCount = recipeData.GetAllRecipes().Count;
            var result = controller.Create(recipe, null) as RedirectToActionResult;
            int newRecipeID = recipeData.GetLastRecipeID();

            Assert.NotNull(result);
            Assert.AreEqual("Details", result.ActionName);
            Assert.AreNotEqual(recipeData.GetAllRecipes().Count, oldRecipeCount);
            Assert.AreEqual(recipeData.GetAllRecipes().Count, oldRecipeCount + 1);

            recipeData.DeleteRecipe(recipeData.GetRecipe(newRecipeID));
        }

        [Test]
        public void RecipeCreateShouldWriteNewRecipeWithImageToDatabaseIfModelIsValid()
        {
            controller = new RecipeController();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            RecipeDAL recipeData = new RecipeDAL();
            List<RecipeIngredient> _ingredients = new List<RecipeIngredient>();
            List<Instruction> _instructions = new List<Instruction>();

            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 cup",
                Ingredient = "Sugar"
            });

            _instructions.Add(new Instruction
            {
                Step = 1,
                InstructionText = "Make food"
            });

            Recipe recipe = new Recipe
            {
                Title = "Test",
                Description = "Test description",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            var ms = new MemoryStream();
            IFormFile image = new FormFile(ms, 0, 0, "image", "applepie.jpg");


            int oldRecipeCount = recipeData.GetAllRecipes().Count;
            var result = controller.Create(recipe, image) as RedirectToActionResult;
            int newRecipeID = recipeData.GetLastRecipeID();

            Assert.NotNull(result);
            Assert.AreEqual("Details", result.ActionName);
            Assert.AreNotEqual(recipeData.GetAllRecipes().Count, oldRecipeCount);
            Assert.AreEqual(recipeData.GetAllRecipes().Count, oldRecipeCount + 1);

            recipeData.DeleteRecipe(recipeData.GetRecipe(newRecipeID));
        }
    }
}
