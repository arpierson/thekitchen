﻿using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.Models;

namespace TheKitchenTests.ControllerTests.RecipeControllerTests
{
    [TestFixture]
    public class RecipeControllerIndexTest
    {
        [Test]
        public void RecipeIndexShouldReturnIndexViewWithRecipeList()
        {
            RecipeController controller = new RecipeController();

            ViewResult result = controller.Index() as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("Index"));

            var model = result.ViewData.Model;
            Assert.IsInstanceOf<System.Collections.Generic.List<Recipe>>(model);
        }
    }
}
