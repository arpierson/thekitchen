﻿using System.Collections.Specialized;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using TheKitchen.Controllers;

namespace TheKitchenTests.ControllerTests.RecipeControllerTests
{
    [TestFixture]
    public class RecipeControllerIngredientSearchTest
    {

        [Test]
        public void RecipeControllerIngredientSearchWithParametersShouldReturnJsonResult()
        {
            RecipeController controller = new RecipeController();
            IActionResult jsonResult = controller.IngredientSearch("eg") as JsonResult;
            Assert.IsNotNull(jsonResult);
        }

        [Test]
        public void RecipeControllerIngredientSearchWithOutParametersShouldReturnJsonResult()
        {
            RecipeController controller = new RecipeController()
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext()
                }
            };
            controller.HttpContext.Request.QueryString = new QueryString("?or");

            IActionResult jsonResult = controller.IngredientSearch(null) as JsonResult;
            Assert.IsNotNull(jsonResult);
        }
    }
}
