﻿using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using TheKitchen.Controllers;

namespace TheKitchenTests.ControllerTests.RecipeControllerTests
{
    [TestFixture]
    public class RecipeControllerGetCreateTests
    {
        [Test]
        public void RecipeIndexShouldReturnCreateView()
        {
            RecipeController controller = new RecipeController();

            ViewResult result = controller.Create() as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("Create"));
        }
    }
}
