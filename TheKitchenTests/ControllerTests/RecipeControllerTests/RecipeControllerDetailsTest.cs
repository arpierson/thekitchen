﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.Models;

namespace TheKitchenTests.ControllerTests.RecipeControllerTests
{
    [TestFixture]
    public class RecipeControllerDetailsTest
    {
        [Test]
        [TestCase(1)]
        [TestCase(2)]
        public void RecipeDetailsShouldReturnRecipeDetailsView(int id)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            tempData["CurrentUserID"] = 1;
            var controller = new RecipeController();

            ClaimsPrincipal claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };
            controller.TempData = tempData;


            ViewResult result = controller.Details(id) as ViewResult;
            Assert.NotNull(result);
            Assert.That(result.ViewName, Is.EqualTo("Details"));

            var model = result.ViewData.Model;
            Assert.IsInstanceOf<Recipe>(model);
        }

        [Test]
        [TestCase(12345)]
        [TestCase(98765)]
        public void RecipeDetailsShouldReturnNotFoundIfRecipeDoesNotExist(int id)
        {
            RecipeController controller = new RecipeController();

            ViewResult result = controller.Details(id) as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("NotFound"));
            Assert.Null(result.Model);
        }
    }
}
