﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.ControllerTests.RecipeControllerTests
{
    [TestFixture]
    public class RecipeControllerRateRecipeTest
    {
        RecipeController recipeController;
        UserController userController;
        int recipeID = -1;

        [OneTimeSetUp]
        public void Init()
        {
            this.userController = new UserController();
            this.recipeController = new RecipeController();
            ClaimsPrincipal claimsUser = new ClaimsPrincipal();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));

            this.recipeController.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            this.userController.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            List<RecipeIngredient> ingredients = new List<RecipeIngredient>();
            ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 cup",
                Ingredient = "Ingredient Name"
            });

            List<Instruction> instructions = new List<Instruction>();
            instructions.Add(new Instruction
            {
                Step = 1,
                InstructionText = "Make food"
            });

            UserDAL userDAL = new UserDAL();
            Recipe recipe = new Recipe
            {
                Title = "Recipe Name",
                Description = "Recipe Description",
                Ingredients = ingredients,
                Directions = instructions
        };

            RecipeDAL dal = new RecipeDAL();
            this.recipeID = dal.CreateRecipe(
                recipe,
                userDAL.GetUser("bdepoin1@my.westga.edu").UserId,
                DateTime.Now);
        }

        [OneTimeTearDown]
        public void FinishTests()
        {
            RecipeDAL dal = new RecipeDAL();
            dal.DeleteRecipe(this.recipeID);
        }

        [Test]
        [TestCase(5)]
        [TestCase(4)]
        [TestCase(3)]
        [TestCase(2)]
        [TestCase(1)]
        public void RateRecipeTasteShouldSetUserRating(int rating)
        {
            var result1 = this.recipeController.TasteRating(this.recipeID, rating) as ViewResult;
            Assert.AreEqual(result1.ViewName, "Details");

            var result2 = this.userController.GetRecipeTasteRating(this.recipeID) as OkObjectResult;
            Assert.AreEqual(result2.StatusCode, 200);
            Assert.AreEqual(result2.Value, rating);
        }

        [Test]
        [TestCase(5)]
        [TestCase(4)]
        [TestCase(3)]
        [TestCase(2)]
        [TestCase(1)]
        public void RateRecipeDificultyShouldSetUserRating(int rating)
        {
            var result1 = this.recipeController.DifficultyRating(this.recipeID, rating) as ViewResult;
            Assert.AreEqual(result1.ViewName, "Details");

            var result2 = this.userController.GetRecipeDifficultyRating(this.recipeID) as OkObjectResult;
            Assert.AreEqual(result2.StatusCode, 200);
            Assert.AreEqual(result2.Value, rating);
        }

        [Test]
        public void UserShouldNotBeAbleToRateDificultyOfThierRecipe()
        {
            var result1 = this.recipeController.DifficultyRating(1, 5)
                as UnauthorizedObjectResult;
            Assert.AreEqual(result1.StatusCode, 401);
        }

        [Test]
        public void UserShouldNotBeAbleToRateTasteOfThierRecipe()
        {
            var result1 = this.recipeController.TasteRating(1, 5)
                as UnauthorizedObjectResult;
            Assert.AreEqual(result1.StatusCode, 401);
        }
    }
}
