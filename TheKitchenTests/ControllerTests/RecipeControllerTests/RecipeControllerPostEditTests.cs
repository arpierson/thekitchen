﻿using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using TheKitchen.Controllers;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.ControllerTests.RecipeControllerTests
{
    [TestFixture]
    public class RecipeControllerPostEditTests
    {
        RecipeController controller;
        ClaimsPrincipal claimsUser;

        [Test]
        public void RecipeEditShouldRedirectToEditViewIfModelIsInvalid()
        {
            controller = new RecipeController();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            Recipe recipe = new Recipe();
            controller.ModelState.AddModelError("Title", @"Title is required");

            ViewResult result = controller.Edit(recipe, null, 1) as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("Edit"));
        }

        [Test]
        public void RecipeEditShouldWriteEditRecipeToDatabaseIfModelIsValid()
        {
            controller = new RecipeController();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            RecipeDAL recipeData = new RecipeDAL();
            List<RecipeIngredient> _ingredients = new List<RecipeIngredient>();
            List<Instruction> _instructions = new List<Instruction>();

            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 cup",
                Ingredient = "Sugar"
            });

            _instructions.Add(new Instruction
            {
                Step = 1,
                InstructionText = "Make food"
            });

            Recipe oldRecipe = new Recipe
            {
                Title = "Test",
                Description = "Test description",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            controller.Create(oldRecipe, null); 
            int oldRecipeID = recipeData.GetLastRecipeID();

            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 oz",
                Ingredient = "Water"
            });

            _instructions.Add(new Instruction
            {
                Step = 2,
                InstructionText = "Eat food"
            });

            Recipe newRecipe = new Recipe
            {
                Title = "Edited title",
                Description = "Test description edited",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            var result = controller.Edit(newRecipe, null, oldRecipeID) as RedirectToActionResult;
            Assert.NotNull(result);
            Assert.AreEqual("Details", result.ActionName);

            recipeData.DeleteRecipe(recipeData.GetRecipe(oldRecipeID));
        }

        [Test]
        public void RecipeEditShouldEditRecipeWithImageToDatabaseIfModelIsValid()
        {
            controller = new RecipeController();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            RecipeDAL recipeData = new RecipeDAL();
            List<RecipeIngredient> _ingredients = new List<RecipeIngredient>();
            List<Instruction> _instructions = new List<Instruction>();

            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 cup",
                Ingredient = "Sugar"
            });

            _instructions.Add(new Instruction
            {
                Step = 1,
                InstructionText = "Make food"
            });

            Recipe oldRecipe = new Recipe
            {
                Title = "Test",
                Description = "Test description",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            controller.Create(oldRecipe, null);
            int oldRecipeID = recipeData.GetLastRecipeID();

            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 oz",
                Ingredient = "Water"
            });

            _instructions.Add(new Instruction
            {
                Step = 2,
                InstructionText = "Eat food"
            });

            Recipe newRecipe = new Recipe
            {
                Title = "Edited title",
                Description = "Test description edited",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            var ms = new MemoryStream();
            IFormFile image = new FormFile(ms, 0, 0, "image", "applepie.jpg");

            var result = controller.Edit(newRecipe, image, oldRecipeID) as RedirectToActionResult;
            Assert.NotNull(result);
            Assert.AreEqual("Details", result.ActionName);

            recipeData.DeleteRecipe(recipeData.GetRecipe(oldRecipeID));
        }

        [Test]
        public void RecipeEditShouldEditTitleButNotIngredientsOrInstructionsIfIngredientsAndInstructionsUnchanged()
        {
            controller = new RecipeController();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            RecipeDAL recipeData = new RecipeDAL();
            List<RecipeIngredient> _ingredients = new List<RecipeIngredient>();
            List<Instruction> _instructions = new List<Instruction>();

            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 cup",
                Ingredient = "Sugar"
            });

            _instructions.Add(new Instruction
            {
                Step = 1,
                InstructionText = "Make food"
            });

            Recipe oldRecipe = new Recipe
            {
                Title = "Test",
                Description = "Test description",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            controller.Create(oldRecipe, null);
            int oldRecipeID = recipeData.GetLastRecipeID();

            Recipe newRecipe = new Recipe
            {
                Title = "Edited title",
                Description = "Test description edited",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            var ms = new MemoryStream();
            IFormFile image = new FormFile(ms, 0, 0, "image", "applepie.jpg");

            var result = controller.Edit(newRecipe, image, oldRecipeID) as RedirectToActionResult;
            Assert.NotNull(result);
            Assert.AreEqual("Details", result.ActionName);

            recipeData.DeleteRecipe(recipeData.GetRecipe(oldRecipeID));
        }

        [Test]
        public void RecipeEditShouldEditIngredientsAndInstructionsIfSameListLenghtButDifferentContents()
        {
            controller = new RecipeController();

            claimsUser = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "TestGuy"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email,"jrobbin4@my.westga.edu")
            }, "mock"));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = claimsUser }
            };

            RecipeDAL recipeData = new RecipeDAL();
            List<RecipeIngredient> _ingredients = new List<RecipeIngredient>();
            List<Instruction> _instructions = new List<Instruction>();

            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 cup",
                Ingredient = "Sugar"
            });

            _instructions.Add(new Instruction
            {
                Step = 1,
                InstructionText = "Make food"
            });

            Recipe oldRecipe = new Recipe
            {
                Title = "Test",
                Description = "Test description",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            controller.Create(oldRecipe, null);
            int oldRecipeID = recipeData.GetLastRecipeID();

            _ingredients.Clear();
            _instructions.Clear();

            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 oz",
                Ingredient = "Water"
            });

            _instructions.Add(new Instruction
            {
                Step = 2,
                InstructionText = "Eat food"
            });

            Recipe newRecipe = new Recipe
            {
                Title = "Edited title",
                Description = "Test description edited",
                Ingredients = _ingredients,
                Directions = _instructions
            };


            var ms = new MemoryStream();
            IFormFile image = new FormFile(ms, 0, 0, "image", "applepie.jpg");

            var result = controller.Edit(newRecipe, image, oldRecipeID) as RedirectToActionResult;
            Assert.NotNull(result);
            Assert.AreEqual("Details", result.ActionName);

            recipeData.DeleteRecipe(recipeData.GetRecipe(oldRecipeID));
        }
    }
}