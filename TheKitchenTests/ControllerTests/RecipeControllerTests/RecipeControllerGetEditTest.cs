﻿using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using TheKitchen.Controllers;

namespace TheKitchenTests.ControllerTests.RecipeControllerTests
{
    [TestFixture]
    public class RecipeControllerGetEditTests
    {
        [Test]
        [TestCase(1)]
        [TestCase(2)]
        public void RecipeIndexShouldReturnEditView(int id)
        {
            RecipeController controller = new RecipeController();

            ViewResult result = controller.Edit(id) as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("Edit"));
        }
    }
}
