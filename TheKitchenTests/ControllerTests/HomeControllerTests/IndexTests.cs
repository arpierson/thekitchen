﻿using NUnit.Framework;
using Microsoft.AspNetCore.Mvc;
using TheKitchen.Controllers;
using Microsoft.Extensions.Logging;

namespace TheKitchenTests.ControllerTests.HomeControllerTests
{
    [TestFixture]
    public class IndexTests
    {
        [Test]
        public void ShouldReturnNameLoginTest()
        {
            HomeController homeController = new HomeController();
            Assert.That(homeController, Is.Not.Null);

            ViewResult result = homeController.Index() as ViewResult;
            Assert.That(result.ViewName, Is.EqualTo("Index"));
        }
    }
}
