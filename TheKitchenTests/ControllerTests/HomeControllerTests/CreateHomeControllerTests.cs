﻿using NUnit.Framework;
using TheKitchen.Controllers;
using Microsoft.Extensions.Logging;

namespace TheKitchenTests.ControllerTests.HomeControllerTests
{
    [TestFixture]
    public class CreateHomeControllerTests
    {
        [Test]
        public void ShouldCreateNewHomeControllerTest()
        {
            HomeController homeController = new HomeController();
            Assert.That(homeController, Is.Not.Null);
        }
    }
}
