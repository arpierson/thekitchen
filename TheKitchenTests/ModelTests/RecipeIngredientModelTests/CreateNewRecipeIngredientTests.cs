﻿
using NUnit.Framework;
using TheKitchen.Models;

namespace TheKitchenTests.ModelTests.RecipeIngredientModelTests
{
    [TestFixture]
    public class CreateNewRecipeIngredientTests
    {
        [Test]
        public void ShouldCreateNewRecipeIngredient()
        {
            RecipeIngredient rI = new RecipeIngredient();
            Assert.That(rI, Is.Not.Null);
        }

        [Test]
        [TestCase(1, "1 cup", "Sugar")]
        [TestCase(1, "2 tsp.", "butter")]
        public void ShouldCreateNewRecipeIngredientAndSetProperties(int id, string measurement, string ingredient)
        {
            RecipeIngredient rI = new RecipeIngredient
            {
                Id = id,
                Measurement = measurement,
                Ingredient = ingredient
            };

            Assert.That(rI, Is.Not.Null);
            Assert.AreEqual(id, rI.Id);
            Assert.AreEqual(measurement, rI.Measurement);
            Assert.AreEqual(ingredient, rI.Ingredient);
        }
    }
}
