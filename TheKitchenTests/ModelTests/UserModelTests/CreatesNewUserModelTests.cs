﻿using NUnit.Framework;
using System;
using TheKitchen.Models;

namespace TheKitchenTests.ModelTests
{
    [TestFixture]
    public class CreatesNewUserModelTests
    {

        [Test]
        public void ShouldCreateNewModelTest()
        {
            UserModel userModel = new UserModel();
            Assert.That(userModel, Is.Not.Null);
        }

        [Test]
        public void ShouldCreateNewUserModelAndSetPropertiesTest()
        {
            DateTime dateAdded = DateTime.Now;
            DateTime dateModified = DateTime.Now;
            DateTime dateDeleted = DateTime.Now;

            UserModel userModel = new UserModel()
            {
                UserId = 1,
                FirstName = "Lily",
                LastName = "Robbins",
                Email = "lily@lily",
                CountryCode = "USA",
                Password = "Lily1234",
                DateAdded = dateAdded,
                DateModified = dateModified,
                Datedeleted = dateDeleted,
            };

            int userId = 1;
            string firstName = "Lily";
            string lastName = "Robbins";
            string email = "lily@lily";
            string country = "USA";
            string password = "Lily1234";

            Assert.That(userModel, Is.Not.Null);
            Assert.AreEqual(userId, userModel.UserId);
            Assert.AreEqual(firstName, userModel.FirstName);
            Assert.AreEqual(lastName, userModel.LastName);
            Assert.AreEqual(email, userModel.Email);
            Assert.AreEqual(country, userModel.CountryCode);
            Assert.AreEqual(password, userModel.Password);
            Assert.AreEqual(dateAdded, userModel.DateAdded);
            Assert.AreEqual(dateModified, userModel.DateModified);
            Assert.AreEqual(dateDeleted, userModel.Datedeleted);
        }
    }
}