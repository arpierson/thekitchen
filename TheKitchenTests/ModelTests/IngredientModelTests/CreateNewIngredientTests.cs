﻿
using NUnit.Framework;
using TheKitchen.Models;

namespace TheKitchenTests.ModelTests.IngredientModelTests
{
    [TestFixture]
    public class CreateNewIngredientTests
    {
        [Test]
        [TestCase("Butter")]
        [TestCase("Chocolate chips")]
        public void ShouldCreateNewIngredient(string name)
        {
            Ingredient i = new Ingredient(name);
            Assert.That(i, Is.Not.Null);
            Assert.AreEqual(i.Name, name);
        }

        [Test]
        [TestCase("Butter", 1)]
        [TestCase("Chocolate chips", 2)]
        public void ShouldCreateNewIngredientAndSetAllProperties(string name, int id)
        {
            Ingredient i = new Ingredient(name);
            i.Id = id;
            Assert.That(i, Is.Not.Null);
            Assert.AreEqual(i.Name, name);
            Assert.AreEqual(i.Id, id);
        }
    }
}
