﻿using NUnit.Framework;
using TheKitchen.Models;

namespace TheKitchenTests.ModelTests.InstructionModelTests
{
    [TestFixture]
    public class CreateNewInstructionTests
    {
        [Test]
        public void ShouldCreateNewInstruction()
        {
            Instruction recipe = new Instruction();
            Assert.That(recipe, Is.Not.Null);
        }

        [Test]
        [TestCase(1, "Preaheat oven to 350")]
        [TestCase(2, "Mix all ingredients")]
        public void ShouldCreateNewInstructionAndSetAllProperties(int step, string text)
        {
            Instruction i = new Instruction
            {
                Step = step,
                InstructionText = text
            };
            Assert.That(i, Is.Not.Null);
            Assert.AreEqual(i.Step, step);
            Assert.AreEqual(i.InstructionText, text);
        }
    }
}
