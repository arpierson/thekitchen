﻿using System.Collections.Generic;
using NUnit.Framework;
using TheKitchen.Models;

namespace TheKitchenTests.ModelTests.RecipeModelTests
{
    [TestFixture]
    public class CreateNewRecipeTests
    {
        private List<RecipeIngredient> _ingredients = new List<RecipeIngredient>();
        private List<Instruction> _instructions = new List<Instruction>();

        [SetUp]
        public void FixtureSetUp()
        {
            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 cup",
                Ingredient = "Sugar"
            });
            _instructions.Add(new Instruction
            {
                Step = 1,
                InstructionText = "Make food"
            });
        }

        [Test]
        public void ShouldCreateNewRecipe()
        {
            Recipe recipe = new Recipe();
            Assert.That(recipe, Is.Not.Null);
        }

        [Test]
        public void ShouldCreateRecipeAndSetProperties()
        {
            Recipe recipe = new Recipe
            {
                Id = 1,
                Title = "Cookies",
                Description = "Yummy cookies",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            int id = 1;
            string title = "Cookies";
            string description = "Yummy cookies";

            Assert.That(recipe, Is.Not.Null);
            Assert.AreEqual(id, recipe.Id);
            Assert.AreEqual(title, recipe.Title);
            Assert.AreEqual(description, recipe.Description);
            Assert.That(recipe.Ingredients, Is.Not.Empty);
            Assert.That(recipe.Ingredients, Is.Not.Null);
            Assert.That(recipe.Directions, Is.Not.Empty);
            Assert.That(recipe.Directions, Is.Not.Null);
        }
    }
}
