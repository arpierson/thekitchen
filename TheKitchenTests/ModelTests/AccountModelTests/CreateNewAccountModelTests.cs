﻿using System.Collections.Generic;
using NUnit.Framework;
using TheKitchen.Models;

namespace TheKitchenTests.ModelTests.AccountModelTests
{

    [TestFixture]
    public class CreateNewAccountModelTests
    {
        
        public void CreateAccountModelTest()
        {
            AccountModel accountModel = new AccountModel();
            Assert.That(accountModel, Is.Not.Null);
        }

        [Test]
        public void ShouldCreateAccountModelAndSetPropertiesTest()
        {
            AccountModel accountModel = new AccountModel()
            {
                User = new UserModel(),
                Recipes = new List<Recipe>(),
            };

            Assert.That(accountModel, Is.Not.Null);
            Assert.That(accountModel.User, Is.Not.Null);
        }
    }
}
