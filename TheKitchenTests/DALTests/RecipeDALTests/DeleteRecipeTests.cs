﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.DALTests.RecipeDALTests
{
    [TestFixture]
    public class DeleteRecipeTests
    {
        private List<RecipeIngredient> _ingredients = new List<RecipeIngredient>();
        private List<Instruction> _instructions = new List<Instruction>();

        [SetUp]
        public void FixtureSetUp()
        {
            _ingredients.Add(new RecipeIngredient
            {
                Measurement = "1 cup",
                Ingredient = "Sugar"
            });
            _instructions.Add(new Instruction
            {
                Step = 1,
                InstructionText = "Make food"
            });
        }

        [Test]
        public void DeleteRecipeShouldDeleteRecipe()
        {
            RecipeDAL dal = new RecipeDAL();
            Recipe recipe = new Recipe
            {
                Title = "Cookies",
                Description = "Yummy cookies",
                Ingredients = _ingredients,
                Directions = _instructions
            };

            int oldRecipeCount = dal.GetAllRecipes().Count;
            int newRecipeID = dal.CreateRecipe(recipe, 1, DateTime.Now);
            Assert.AreNotEqual(dal.GetAllRecipes().Count, oldRecipeCount);

            dal.DeleteRecipe(newRecipeID);
            Assert.AreEqual(dal.GetAllRecipes().Count, oldRecipeCount);
        }
    }
}
