﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.DALTests.RecipeDALTests
{
    [TestFixture]
    public class GetRecipesTests
    {
        int userID = -1;

        [SetUp]
        public void TestSetUp()
        {
            UserDAL userDAL = new UserDAL();
            this.userID = userDAL.GetUser("jrobbin4@my.westga.edu").UserId;
        }

        [Test]
        public void GetAllRecipesNoParamsShouldReturnAllRecipesUpTo1000000()
        {
            RecipeDAL dal = new RecipeDAL();
            List<Recipe> recipes = dal.GetAllRecipes();
            int allRecipeCount = dal.GetTotalRecipeCount();

            Assert.AreEqual(recipes.Count, allRecipeCount);
        }

        [Test]
        public void GetAllRecipesUser1ShouldReturnOnlyUser1Recipes()
        {
            RecipeDAL dal = new RecipeDAL();
            List<Recipe> recipes = dal.GetAllRecipes(1);

            foreach (Recipe recipe in recipes)
            {
                Assert.AreEqual(recipe.UserId, 1);
            }  
        }

        [Test]
        public void GetAllRecipesUserId1CountShouldEqualGetTotalRecipeCountUserID1Count()
        {
            RecipeDAL dal = new RecipeDAL();
            List<Recipe> recipes = dal.GetAllRecipes(1);
            int allRecipeCount = dal.GetTotalRecipeCount(1);

            Assert.AreEqual(recipes.Count, allRecipeCount);
        }

        [Test]
        public void GetAllRecipesSortByTasteDESCShouldListHighestRecipesFirst()
        {
            string sortOrderBy = TheKitchen.Utilities.Recipe.BuildRecipeOrderBy(
                "taste", "highToLow");
            RecipeDAL dal = new RecipeDAL();
            List<Recipe> recipes = dal.GetAllRecipes(sortOrderBy, 0, 2400, String.Empty);
            int previousRating = 5;

            foreach (Recipe recipe in recipes)
            {
                Assert.LessOrEqual(recipe.TasteRating, previousRating);
                previousRating = recipe.TasteRating;
            }
        }

        [Test]
        public void GetAllRecipesSortByTasteASCShouldListLowestRecipesFirst()
        {
            string sortOrderBy = TheKitchen.Utilities.Recipe.BuildRecipeOrderBy(
                "taste", "lowToHigh");
            RecipeDAL dal = new RecipeDAL();
            List<Recipe> recipes = dal.GetAllRecipes(sortOrderBy, 0, 2400, String.Empty);
            int previousRating = 0;

            foreach (Recipe recipe in recipes)
            {
                Assert.GreaterOrEqual(recipe.TasteRating, previousRating);
                previousRating = recipe.TasteRating;
            }
        }

        [Test]
        public void GetAllRecipesSortByDifficultyDESCShouldListHighestRecipesFirst()
        {
            string sortOrderBy = TheKitchen.Utilities.Recipe.BuildRecipeOrderBy(
                "difficulty", "highToLow");
            RecipeDAL dal = new RecipeDAL();
            List<Recipe> recipes = dal.GetAllRecipes(sortOrderBy, 0, 2400, String.Empty);
            int previousRating = 5;

            foreach (Recipe recipe in recipes)
            {
                Assert.LessOrEqual(recipe.DifficultyRating, previousRating);
                previousRating = recipe.DifficultyRating;
            }
        }

        [Test]
        public void GetAllRecipesSortByDifficultyASCShouldListLowestRecipesFirst()
        {
            string sortOrderBy = TheKitchen.Utilities.Recipe.BuildRecipeOrderBy(
                "difficulty", "lowToHigh");
            RecipeDAL dal = new RecipeDAL();
            List<Recipe> recipes = dal.GetAllRecipes(sortOrderBy, 0, 2400, String.Empty);
            int previousRating = 0;

            foreach (Recipe recipe in recipes)
            {
                Assert.GreaterOrEqual(recipe.DifficultyRating, previousRating);
                previousRating = recipe.DifficultyRating;
            }
        }

        [Test]
        [TestCase(1)]
        [TestCase(2)]
        public void GetAllRecipesPageSize(int pageSize)
        {
            RecipeDAL dal = new RecipeDAL();
            List<Recipe> recipes = dal.GetAllRecipes(String.Empty, 0, pageSize, String.Empty);
            Assert.AreEqual(recipes.Count, pageSize);
        }

        private List<Recipe> previousRecipes = new List<Recipe>();
        private int pageCount = 1;

        [Test]
        [TestCase(1,2)]
        [TestCase(2, 2)]
        [TestCase(3, 2)]
        [TestCase(4, 2)]
        public void GetAllRecipesPaging(int page, int pageSize)
        {
            RecipeDAL dal = new RecipeDAL();
            page = TheKitchen.Utilities.Recipe.SetPage(page);
            List<Recipe> recipes = dal.GetAllRecipes(String.Empty, page, pageSize, String.Empty);
            foreach(Recipe recipe in recipes)
            {
                Assert.IsFalse(this.previousRecipes.Exists(r => r.Id == recipe.Id));
                this.previousRecipes.Add(recipe);
            }
        }

        [Test]
        [TestCase(1, 2)]
        [TestCase(2, 2)]
        [TestCase(3, 2)]
        [TestCase(4, 2)]
        [TestCase(3, 2)]
        [TestCase(2, 2)]
        [TestCase(1, 2)]
        public void GetAllRecipesPagingBackwards(int page, int pageSize)
        {
            if (this.pageCount == 1)
            {
                this.previousRecipes = new List<Recipe>();
            }

            RecipeDAL dal = new RecipeDAL();
            this.pageCount += 1;
            page = TheKitchen.Utilities.Recipe.SetPage(page);
            List<Recipe> recipes = dal.GetAllRecipes(String.Empty, page, pageSize, String.Empty);
            this.previousRecipes.AddRange(recipes);

            if (this.pageCount > 4)
            {
                foreach (Recipe recipe in recipes)
                {
                    Assert.IsTrue(this.previousRecipes.Exists(r => r.Id == recipe.Id));   
                }
            }
        }

        [Test]
        [TestCase(
            "Test Recipe",
            "Test Recipe Description",
            "Test Recipe Ingredient",
            "Test Recipe",
            1)]
        [TestCase(
            "Neat Recipe",
            "Neat Recipe Description",
            "Neat Recipe Ingredient",
            "Neat",
            2)]
        [TestCase(
            "Swell Recipe",
            "Swell Recipe Description",
            "Swell Recipe Ingredient",
            "Swell",
            3)]
        [TestCase(
            "Sho Nuff Good Nuff Recipe",
            "Sho Nuff Good Nuff Recipe Description",
            "Sho Nuff Good Nuff Recipe Ingredient",
            "Sho Nuff",
            5)]
        [TestCase(
            "You know my steez Recipe",
            "You know my steez Description",
            "You know my steez Ingredient",
            "steez",
            8)]
        public void GetAllRecipesSearchShouldReturnNewRecipesMatchingCase(
            string recipeName,
            string recipeDescription,
            string ingredientName,
            string searchPhrase,
            int recipesToMake)
        {
            RecipeDAL dal = new RecipeDAL();

            for (int i = 0; i < recipesToMake; i++)
            {
                List<RecipeIngredient> ingredients = new List<RecipeIngredient>();
                ingredients.Add(new RecipeIngredient
                {
                    Measurement = "1 cup",
                    Ingredient = ingredientName + i
                }); 

                List<Instruction> instructions = new List<Instruction>();
                instructions.Add(new Instruction
                {
                    Step = 1,
                    InstructionText = "Make food"
                });

                Recipe recipe = new Recipe
                {
                    Title = recipeName + i,
                    Description = recipeDescription + i,
                    Ingredients = ingredients,
                    Directions = instructions
                };

                dal.CreateRecipe(recipe, 1, DateTime.Now);
            }

            List<Recipe> recipes = dal.GetAllRecipes(String.Empty, 1, 24, searchPhrase);
            foreach (Recipe recipe in recipes)
            {
                Assert.IsTrue(recipe.Title.Contains(searchPhrase));
                Assert.IsTrue(recipe.Description.Contains(searchPhrase));
            }

            foreach (Recipe recipe in recipes)
            {
                dal.DeleteRecipe(recipe.Id);
            }
        }

        [Test]
        [TestCase(
            "Test Recipe",
            "Test Recipe Description",
            "Test Recipe Ingredient",
            "tESt RecIpe",
            1)]
        [TestCase(
            "Neat Recipe",
            "Neat Recipe Description",
            "Neat Recipe Ingredient",
            "NeAt",
            2)]
        [TestCase(
            "Swell Recipe",
            "Swell Recipe Description",
            "Swell Recipe Ingredient",
            "SWEll",
            3)]
        [TestCase(
            "Sho Nuff Good Nuff Recipe",
            "Sho Nuff Good Nuff Recipe Description",
            "Sho Nuff Good Nuff Recipe Ingredient",
            "sho nUff",
            5)]
        [TestCase(
            "You know my steez Recipe",
            "You know my steez Description",
            "You know my steez Ingredient",
            "sTeez",
            8)]
        public void GetAllRecipesSearchShouldReturnNewRecipesInsensitiveCase(
            string recipeName,
            string recipeDescription,
            string ingredientName,
            string searchPhrase,
            int recipesToMake)
        {
            RecipeDAL dal = new RecipeDAL();

            for (int i = 0; i < recipesToMake; i++)
            {
                List<RecipeIngredient> ingredients = new List<RecipeIngredient>();
                ingredients.Add(new RecipeIngredient
                {
                    Measurement = "1 cup",
                    Ingredient = ingredientName + i
                });

                List<Instruction> instructions = new List<Instruction>();
                instructions.Add(new Instruction
                {
                    Step = 1,
                    InstructionText = "Make food"
                });

                Recipe recipe = new Recipe
                {
                    Title = recipeName + i,
                    Description = recipeDescription + i,
                    Ingredients = ingredients,
                    Directions = instructions
                };

                dal.CreateRecipe(recipe, 1, DateTime.Now);
            }

            List<Recipe> recipes = dal.GetAllRecipes(String.Empty, 1, 24, searchPhrase);
            foreach (Recipe recipe in recipes)
            {
                Assert.IsTrue(recipe.Title.ToLower().Contains(searchPhrase.ToLower()));
                Assert.IsTrue(recipe.Description.ToLower().Contains(searchPhrase.ToLower()));
            }

            foreach (Recipe recipe in recipes)
            {
                dal.DeleteRecipe(recipe.Id);
            }
        }
    }
}
