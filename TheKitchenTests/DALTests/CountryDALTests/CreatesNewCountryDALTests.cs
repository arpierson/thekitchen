﻿using NUnit.Framework;
using TheKitchen.DAL;

namespace TheKitchenTests.DALTests.CountryDALTests
{

    [TestFixture]
    public class CreatesNewCountryDALTests
    {

        [Test]
        public void ShouldCreateNewCountryDAL()
        {
            CountryDAL countryDAL = new CountryDAL();
            Assert.That(countryDAL, Is.Not.Null);
        }
    }
}