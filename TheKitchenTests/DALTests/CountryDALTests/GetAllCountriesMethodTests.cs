﻿using NUnit.Framework;
using TheKitchen.DAL;
using Moq;

namespace TheKitchenTests.DALTests.CountryDALTests
{
    [TestFixture]
    public class GetAllCountriesMethodTests
    {

        [Test]
        public void ShouldGetAllCountriesTest()
        {
            CountryDAL countryDAL = new CountryDAL();
            Assert.That(countryDAL, Is.Not.Null);

            Assert.AreEqual(239, countryDAL.GetCountryDataTable().Rows.Count);
        }

        [Test]
        public void ShouldGetAllCountriesWithMockCountryDALObjectTest()
        {
            Mock<CountryDAL> mock = new Mock<CountryDAL>();
            Assert.AreEqual(239, mock.Object.GetCountryDataTable().Rows.Count);
        }

        [Test]
        public void ShouldGetCountryFromCodeTest()
        {
            CountryDAL countryDAL = new CountryDAL();
            string code = "US";

            Assert.AreEqual("United States", countryDAL.GetCountryFromCode(code));
        }

        [Test]
        public void ShouldNotGetCountryFromCodeTest()
        {
            CountryDAL countryDAL = new CountryDAL();
            string code = "ZZ";

            Assert.AreEqual("N/A", countryDAL.GetCountryFromCode(code));
        }
    }
}