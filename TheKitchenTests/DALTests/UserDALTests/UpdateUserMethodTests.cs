﻿using NUnit.Framework;
using System;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.DALTests.UserDALTests
{
    [TestFixture]
    public class UpdateUserMethodTests
    {
        [Test]
        public void ShouldUpdateUsersFirstNameTest()
        {
            UserDAL userDal = new UserDAL();
            UserModel newUser = new UserModel()
            {
                FirstName = "Lily",
                LastName = "Robbins",
                Email = "lily@lily.com",
                CountryCode = "US",
                Password = "Lily1234!"
            };

            userDal.AddUser(newUser);
            UserModel user = userDal.GetUser("lily@lily.com");

            UserModel userToUpdate = new UserModel()
            {
                FirstName = "Lindsey",
                LastName = "Robbins",
                Email = "lily@lily.com",
                CountryCode = "US",
                Password = "Lily1234!"
            };

            userDal.UpdateUser(user, userToUpdate);
            UserModel result = userDal.GetUser("lily@lily.com");

            Assert.AreEqual("Lindsey", result.FirstName);

            userDal.DeleteUser("lily@lily.com");
        }

        [Test]
        public void ShouldUpdateUsersNameCountryTest()
        {
            UserDAL userDal = new UserDAL();
            UserModel newUser = new UserModel()
            {
                FirstName = "Lily",
                LastName = "Robbins",
                Email = "lily@lily.com",
                CountryCode = "US",
                Password = "Lily1234!"
            };

            userDal.AddUser(newUser);
            UserModel user = userDal.GetUser("lily@lily.com");

            UserModel userToUpdate = new UserModel()
            {
                FirstName = "Lindsey",
                LastName = "Ford",
                Email = "lily@lily.com",
                CountryCode = "LI",
                Password = "Lindsey1234!"
            };

            userDal.UpdateUser(user, userToUpdate);
            UserModel result = userDal.GetUser("lily@lily.com");

            Assert.AreEqual("Lindsey", result.FirstName);
            Assert.AreEqual("Ford", result.LastName);
            Assert.AreEqual("LI", result.CountryCode);

            userDal.DeleteUser("lily@lily.com");
        }
    }
}