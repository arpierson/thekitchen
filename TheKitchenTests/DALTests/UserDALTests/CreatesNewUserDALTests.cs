﻿using NUnit.Framework;
using TheKitchen.DAL;

namespace TheKitchenTests.DALTests.UserDALTests
{
    [TestFixture]
    public class CreatesNewUserDALTests
    {

        [Test]
        public void ShouldCreateNewUserDAL()
        {
            UserDAL userDAL = new UserDAL();
            Assert.That(userDAL, Is.Not.Null);
        }
    }
}