﻿using NUnit.Framework;
using System;
using TheKitchen.DAL;
using TheKitchen.Models;
using Moq;

namespace TheKitchenTests.DALTests.UserDALTests
{
    [TestFixture]
    public class AddUserTests
    {

        [Test]
        public void ShouldAddUserToDatabaseTest()
        {
            UserModel userModel = new UserModel()
            {
                FirstName = "Lily",
                LastName = "Robbins",
                Email = "lily@lily.com",
                CountryCode = "US",
                Password = "Lily1234",
                DateAdded = DateTime.Now,
            };

            Mock<UserDAL> mock = new Mock<UserDAL>();
            Assert.That(mock.Object.AddUser(userModel).UserId > 1);
            mock.Object.DeleteUser("lily@lily.com");
        }
    }
}