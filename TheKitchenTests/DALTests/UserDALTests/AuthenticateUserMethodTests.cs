﻿using NUnit.Framework;
using TheKitchen.DAL;
using TheKitchen.Models;
using TheKitchen.Controllers;

namespace TheKitchenTests.DALTests.UserDALTests
{
    [TestFixture]
    public class AuthenticateUserMethodTests
    {

        [Test]
        public void ShouldReturnUserTestAsync()
        {
            UserDAL userDAL = new UserDAL();
            Assert.That(userDAL, Is.Not.Null);
            UserController userController = new UserController();
            Assert.That(userController, Is.Not.Null);
            
            UserModel userModel = new UserModel()
            {
                Email = "jrobbin4@my.westga.edu",
                Password = "Kitchen!23",
            };
            Assert.That(userModel, Is.Not.Null);

            string email = "jrobbin4@my.westga.edu";
            string password = "Kitchen!23";

            Assert.AreEqual(userModel.Email, userDAL.AuthenticateUser(email, password).Email);
        }
    }
}