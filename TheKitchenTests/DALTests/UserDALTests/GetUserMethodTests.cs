﻿using NUnit.Framework;
using TheKitchen.DAL;
using TheKitchen.Models;

namespace TheKitchenTests.DALTests.UserDALTests
{
    [TestFixture]
    public class GetUserMethodTests
    {

        [Test]
        public void ShouldReturnUserTest()
        {
            UserDAL userDAL = new UserDAL();
            Assert.That(userDAL, Is.Not.Null);

            UserModel userModel = new UserModel()
            {
                Email = "jrobbin4@my.westga.edu",
            };
            Assert.That(userModel, Is.Not.Null);

            string email = "jrobbin4@my.westga.edu";
            Assert.AreEqual(userModel.Email, userDAL.GetUser(email).Email);
        }
    }
}